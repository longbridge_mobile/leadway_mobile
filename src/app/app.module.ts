import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule } from '@angular/core';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';
import { HttpModule } from '@angular/http';
import { NgIdleModule } from '@ng-idle/core';

import { SplashScreen } from '@ionic-native/splash-screen';
import { StatusBar } from '@ionic-native/status-bar';
import { BackgroundMode } from '@ionic-native/background-mode';
import { CallNumber } from '@ionic-native/call-number';
import { Camera } from '@ionic-native/camera';
import { FingerprintAIO } from '@ionic-native/fingerprint-aio';
import { NativePageTransitions } from '@ionic-native/native-page-transitions';
import { SocialSharing } from '@ionic-native/social-sharing';
import { Device } from '@ionic-native/device';
import { HeaderColor } from '@ionic-native/header-color';
import { Keyboard } from '@ionic-native/keyboard';
import { DatePicker } from '@ionic-native/date-picker';
import { Network } from '@ionic-native/network';
import { TouchID } from '@ionic-native/touch-id';
import { AppVersion } from '@ionic-native/app-version';

import { MyApp } from './app.component';

import { ControllerService } from '../providers/controller-service';
import { NativeService } from '../providers/native-service';
import { StorageService } from '../providers/storage-service';
import { ServerServiceProvider } from '../providers/server-service/server-service';

import { IonDigitKeyboard  } from '../components/ion-digit-keyboard/ion-digit-keyboard';
import { LoginPage } from '../pages/login/login';
import { NewUserDevice } from '../pages/new-user-device/new-user-device';
import { HomePage } from '../pages/home/home';
import { FaqPage } from '../pages/faq/faq';
import { FaqModalPage } from '../pages/faq-modal/faq-modal';
import { ProductsPage } from '../pages/products/products';
import { BranchesPage } from '../pages/branches/branches';
import { ChartPage } from '../pages/chart/chart';
import { ChartModalPage } from '../pages/chart-modal/chart-modal';
import { ProfilePage } from '../pages/profile/profile';
import { FullDetailsPage } from '../pages/full-details/full-details';
import { SettingsPage } from '../pages/settings/settings';
import { SubscriptionPage } from '../pages/subscription/subscription';
import { EditProfilePage } from '../pages/edit-profile/edit-profile';
import { EditEmployerPage } from '../pages/edit-employer/edit-employer';
import { BenefitsPage } from '../pages/benefits/benefits';
import { CalculatorPage } from '../pages/calculator/calculator';
import { CalculatorModalPage } from '../pages/calculator-modal/calculator-modal';
import { NewPinPage } from '../pages/new-pin/new-pin';
import { ConfirmNewPinPage } from '../pages/confirm-new-pin/confirm-new-pin';
import { RegisterDevicePage } from '../pages/register-device/register-device';
import { CalculatorPopPage } from '../pages/calculator-pop/calculator-pop';
import { AccountPage } from '../pages/account/account';
import { TransactionPage } from '../pages/transaction/transaction';
import { SearchTransactionPage } from '../pages/search-transaction/search-transaction';
import { TranDetailPage } from '../pages/tran-detail/tran-detail';
import { TabPage } from '../pages/tab/tab';
import { NotificationPage } from '../pages/notification/notification';
import { ChangePinPage } from '../pages/change-pin/change-pin';
import { HelpPage } from '../pages/help/help';
import { ResetPinPage } from '../pages/reset-pin/reset-pin';
import { AboutPage } from '../pages/about/about';
import { AppDataServiceProvider } from '../providers/app-data-service/app-data-service';
import { SurveyPage } from '../pages/survey/survey';
import { BenefitFormPage } from '../pages/benefit-form/benefit-form';
import { BranchModalPage } from '../pages/branch-modal/branch-modal';
import { BankbranchModalPage } from '../pages/bankbranch-modal/bankbranch-modal';
import { SignaturePadPage } from '../pages/signature-pad/signature-pad';
import { SignaturePadModule } from 'angular2-signaturepad';
import { BranchMapPage } from '../pages/branch-map/branch-map';
import { ViewMessagePage } from '../pages/view-message/view-message';
import { SurveyPipe } from '../pipes/survey/survey';

@NgModule({
  declarations: [
    MyApp,
    IonDigitKeyboard,
    LoginPage,
    NewUserDevice,
    HomePage,
    FaqPage,
    FaqModalPage,
    ProductsPage,
    BranchesPage,
    ChartPage,
    ChartModalPage,
    ProfilePage,
    FullDetailsPage,
    SubscriptionPage,
    EditProfilePage,
    EditEmployerPage,
    BenefitsPage,
    CalculatorPage,
    CalculatorModalPage,
    SettingsPage,
    NewPinPage,
    ConfirmNewPinPage,
    RegisterDevicePage,
    CalculatorPopPage,
    AccountPage,
    TransactionPage,
    TranDetailPage,
    TabPage,
    NotificationPage,
    SearchTransactionPage,
    ChangePinPage,
    HelpPage,
    ResetPinPage,
    AboutPage,
    SurveyPage,
    BenefitFormPage,
    BranchModalPage,
    BankbranchModalPage,
    SignaturePadPage,
    BranchMapPage,
    ViewMessagePage,
    SurveyPipe
  ],
  imports: [
    BrowserModule,
    IonicModule.forRoot(MyApp),
    HttpModule,
    SignaturePadModule,
    NgIdleModule.forRoot()
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    LoginPage,
    NewUserDevice,
    HomePage,
    FaqPage,
    FaqModalPage,
    ProductsPage,
    BranchesPage,
    ChartPage,
    ChartModalPage,
    ProfilePage,
    SettingsPage,
    SubscriptionPage,
    EditProfilePage,
    EditEmployerPage,
    BenefitsPage,
    CalculatorPage,
    CalculatorModalPage,
    SettingsPage,
    NewPinPage,
    ConfirmNewPinPage,
    RegisterDevicePage,
    CalculatorPopPage,
    AccountPage,
    TransactionPage,
    TranDetailPage,
    TabPage,
    NotificationPage,
    SearchTransactionPage,
    ChangePinPage,
    HelpPage,
    ResetPinPage,
    AboutPage,
    SurveyPage,
    BenefitFormPage,
    BranchModalPage,
    BankbranchModalPage,
    SignaturePadPage,
    BranchMapPage,
    ViewMessagePage
  ],
  providers: [
    StatusBar,
    SplashScreen,
    BackgroundMode,
    CallNumber,
    Camera,
    FingerprintAIO,
    NativePageTransitions,
    SocialSharing,
    Device,
    HeaderColor,
    Keyboard,
    DatePicker,
    Network,
    AppVersion,
    TouchID,
    {provide: ErrorHandler, useClass: IonicErrorHandler},
    ControllerService,
    NativeService,
    StorageService,
    ServerServiceProvider,
    AppDataServiceProvider
  ]
})
export class AppModule {}
