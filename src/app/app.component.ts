import { Component } from '@angular/core';
import { Platform, App } from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { StorageService } from '../providers/storage-service';
import { NativeService } from '../providers/native-service';
import { ControllerService } from '../providers/controller-service';
import { ServerServiceProvider } from '../providers/server-service/server-service';
import { AppDataServiceProvider } from '../providers/app-data-service/app-data-service';
import { LoginPage } from '../pages/login/login';
import { NewUserDevice } from '../pages/new-user-device/new-user-device';
import { Network } from '@ionic-native/network';
import { Idle, DEFAULT_INTERRUPTSOURCES } from '@ng-idle/core';

@Component({
  templateUrl: 'app.html'
})
export class MyApp {
  rootPage: any;
  loader: any;

  constructor(
    private platform: Platform, 
    statusBar: StatusBar, 
    private splashScreen: SplashScreen,
    private store: StorageService,
    private native: NativeService,
    private serverService: ServerServiceProvider,
    private controller: ControllerService,
    private appService: AppDataServiceProvider,
    private network: Network,
    app: App,
    idle: Idle
  ) {
    platform.ready().then(() => {
      if (platform.is("ios")) statusBar.overlaysWebView(false);
      statusBar.backgroundColorByHexString('#f8822c');
      this.native.setHeaderColor();
      this.splashScreen.hide();
      this.networkCheck();

      idle.setIdle(150);
      idle.setTimeout(150);
      idle.setInterrupts(DEFAULT_INTERRUPTSOURCES);

      idle.onTimeout.subscribe(() => {
        let view: any = app.getActiveNav();

        if (view._views[0].component != LoginPage && view._views[0].component != NewUserDevice) {
          app.getRootNav().setRoot(LoginPage, {
            timedout: true
          });
          this.clearLoginSession();
        }
        idle.watch();
      });

      idle.onIdleStart.subscribe(() => {
        console.log('idle started');
      });

      idle.watch();

      this.checkDevicePin();
    });
  }

  async checkDevicePin() {
    let loader = this.controller.showLoader('Loading App...');
    loader.present();

    const loginResponse = await this.store.fetchDoc('logindata');
    const deviceResponse = await this.store.fetchDoc('device');

    if (deviceResponse != 'Failed' && deviceResponse.uuid != '' && loginResponse != 'Failed' && loginResponse.pencomPin != '') {
      this.rootPage = LoginPage;
    }
    else {
      await this.getDeviceUUID();
    }

    loader.dismiss();
  }

  async getDeviceUUID() {
    const loginResponse = await this.store.fetchDoc('logindata');

    var serverResponse;
    const deviceResponse = await this.store.fetchDoc('device');

    if (deviceResponse == 'Failed') {
      const deviceUUID = await this.native.getDeviceInfo();
      serverResponse = await this.isDeviceRegistered(deviceUUID);
    }
    else {
      serverResponse = await this.isDeviceRegistered(deviceResponse.uuid);
    }

    try {
      if (serverResponse.StatusCode == '00') {
          if (loginResponse.pencomPin == '' || loginResponse == 'Failed') {
            this.rootPage = NewUserDevice;
          }
          else
          {
            this.rootPage = LoginPage;
          }
      } else if (loginResponse.pencomPin != '' && loginResponse != 'Failed' && serverResponse.StatusCode != 'FF'){
          this.rootPage = LoginPage;
      }
      else {
        this.rootPage = NewUserDevice;
      }
    }
    catch(err) {
      console.log(err);
      if (loginResponse.pencomPin != '') {
        this.rootPage = LoginPage;
      }
      else {
        this.rootPage = NewUserDevice;
      }
    }
  }

  async isDeviceRegistered(deviceId) {
    let data = {
      deviceId: deviceId
    };
    let funcName = '/GetDevice';

    try {
      let response = await this.serverService.processData(data, funcName);
      return response;
    }
    catch(err) {
      console.log(err);
      this.rootPage = NewUserDevice;
    }
  }

  networkCheck() {
    this.network.onDisconnect().subscribe(() => {
      this.controller.showToast('Network disconnected', 'top');
    });

    this.network.onConnect().subscribe(() => {
      this.controller.showToast('Network connected', 'top');
    });
  }

  async clearLoginSession() {
    let response = await this.store.fetchDoc('logindata');

    if (response != 'Failed') {
      response.sessionId = '';
      
      await this.store.createUpdateDoc(response);
    }
  }

}

