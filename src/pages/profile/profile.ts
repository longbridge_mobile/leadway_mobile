import { Component } from '@angular/core';
import { NavController, Platform } from 'ionic-angular';
import { StorageService } from '../../providers/storage-service';
import { ServerServiceProvider } from '../../providers/server-service/server-service';
import { ControllerService } from '../../providers/controller-service';
import { SettingsPage } from '../settings/settings';

@Component({
  selector: 'page-profile',
  templateUrl: 'profile.html',
})
export class ProfilePage {
  image: string = 'assets/images/user.png';
  userName: string = "";
  pencomPin: string = "";
  showMore: boolean = false;
  showLess: boolean = false;

  employer: string = "";
  mobileNo: string = "";
  email: string = "";
  dateOfBirth: string = "";
  fileNo: string = "";
  userAddress: string = "";
  userAddress2: string = "";

  kinName: string = "";
  kinTitle: string = "";
  kinMobile: string = "";
  kinEmail: string = "";
  kinDob: string = "";

  employerAddress: string = "";
  dateOfEmployment: string = "";
  lastFundReceived: string = "";

  constructor(
    private store: StorageService,
    private serverService: ServerServiceProvider,
    private controller: ControllerService,
    private navCtrl: NavController,
    private platform: Platform
  ) {}

  ionViewDidLoad() {
    console.log('ionViewDidLoad ProfilePage');
    this.getFullUserDetails();
  }

  ionViewDidEnter(){
    this.platform.registerBackButtonAction(() => {
      this.navCtrl.parent.select(0);
    });
  }

  async getFullUserDetails() {
    let biometricResponse = await this.store.fetchDoc('biometric');
    if (biometricResponse.image != '' && biometricResponse != 'Failed') {
      this.image = biometricResponse.image;
    }

    let userResponse = await this.store.fetchDoc('user');
    console.log(userResponse);

    let employerResponse = await this.store.fetchDoc('employer');
    console.log(employerResponse);

    let kinResponse = await this.store.fetchDoc('kin');
    console.log(kinResponse);

    if ((userResponse != "Failed" && employerResponse != "Failed" && kinResponse != "Failed") && userResponse.pencomPin != '') {
      this.populateFields(
        userResponse.firstName + " " + userResponse.lastName,
        userResponse.pencomPin,
        userResponse.employer,
        userResponse.mobileNo,
        userResponse.email,
        userResponse.dob,
        userResponse.fileNo,
        userResponse.address,
        userResponse.address2,
        kinResponse.firstName + " " + kinResponse.lastName,
        kinResponse.title,
        kinResponse.mobileNo,
        kinResponse.email,
        kinResponse.dob,
        employerResponse.date,
        employerResponse.address
      );
      this.showLess = true;
      this.showMore = false;
    }
    else {
      this.showLess = false;
      this.showMore = false;
      let loginResponse = await this.store.fetchDoc('logindata');
      console.log(loginResponse);
      let deviceResponse = await this.store.fetchDoc('device');
      await this.fetchFromServer(loginResponse.sessionId, loginResponse.pencomPin, deviceResponse.uuid);
    }
  }

  async fetchFromServer(sessionId, penPin, uuid) {
    let loader = this.controller.showLoader("Loading...");
    loader.present();

    let body = {
      sessionID: sessionId
    };
    var funcName = '/FullClientDetails';

    let data = {
      sessionId: sessionId,
      username: penPin,
      mobileId: uuid
    };
    var funcName2 = '/Biometrics';

    try {
      let serverResponse = await this.serverService.processData(body, funcName);
      let biometricResp = await this.serverService.processData(data, funcName2);
      console.log(serverResponse);
      console.log(biometricResp);

      if (biometricResp.StatusCode == '00') {
        let imageResponse = await this.store.fetchDoc('biometric');
        let imageDoc = {
          _id: 'biometric',
          image: biometricResp.Data.PassportUrl,
          signature: biometricResp.SignatureUrl,
          _rev: imageResponse._rev
        }

        await this.store.createUpdateDoc(imageDoc);
      }

      if (serverResponse.StatusCode == "00") {
        let userResponse = await this.store.fetchDoc('user');
        let empResponse = await this.store.fetchDoc('employer');
        let kinResponse = await this.store.fetchDoc('kin');

        let userDoc = {
          _id: 'user',
          firstName: serverResponse.Data.firstname,
          lastName: serverResponse.Data.surname,
          middleName: serverResponse.Data.othername,
          title: serverResponse.Data.title,
          dob: serverResponse.Data.dateofbirth,
          employer: serverResponse.Data.employername,
          mobileNo: serverResponse.Data.mobileno,
          pencomPin: serverResponse.Data.pencompin,
          email: serverResponse.Data.emailaddres,
          fileNo: serverResponse.Data.filenumber,
          address: serverResponse.Data.residentialaddress1,
          address2: serverResponse.Data.residentialaddress2,
          gender: serverResponse.Data.sex,
          stateOfOrigin: serverResponse.Data.stateoforigjn,
          lga: serverResponse.Data.lgastateoforigin,
          retireeStatus: serverResponse.Data.retiredstatus,
          fundedStatus: serverResponse.Data.fundedstatus,
          maritalStatus: serverResponse.Data.maritalstatus,
          _rev: userResponse._rev
        };

        let employerDoc = {
          _id: 'employer',
          name: serverResponse.Data.employername,
          address: serverResponse.Data.officeaddress1,
          date: serverResponse.Data.date_of_first_employment,
          _rev: empResponse._rev
        };

        let kinDoc = {
          _id: 'kin',
          title: serverResponse.Data.noktitle,
          firstName: serverResponse.Data.nokfirstname,
          lastName: serverResponse.Data.noklastname,
          middleName: serverResponse.Data.nokmiddlename,
          mobileNo: serverResponse.Data.nokphoneno,
          email: serverResponse.Data.nokemail,
          dob: serverResponse.Data.nokdob,
          _rev: kinResponse._rev
        };

        let docResponse = await this.store.bulkCreateDoc([userDoc, employerDoc, kinDoc]);
        console.log(docResponse);
        this.getFullUserDetails();
      }
      else if (serverResponse.StatusCode == "FF") {
        this.controller.showToast(serverResponse.Message + ". Pull down to try again.", "bottom");
      }
      else {
        this.controller.showToast('Unable to fetch profile info. Pull down to refresh.', 'bottom');
      }
    }
    catch(err) {
      console.log(err);
      this.controller.showToast('Unable to fetch profile info. Check your network connection.', 'bottom');
    }
    loader.dismiss();
  }

  populateFields(fullName, pin, employer, mobile, email, dob, fileNo, address, address2, kinName, kinTitle, kinMobile, kinEmail, kinDob, empDate, empAddress) {
    this.userName = fullName;
    this.pencomPin = pin;
    this.employer = employer;
    this.mobileNo = mobile;
    this.email = email;
    this.dateOfBirth = dob;
    this.fileNo = fileNo;
    this.userAddress = address;
    this.userAddress2 = address2;

    this.kinName = kinName;
    this.kinTitle = kinTitle;
    this.kinMobile = kinMobile;
    this.kinEmail = kinEmail;
    this.kinDob = kinDob;

    this.employerAddress = empAddress;
    this.dateOfEmployment = empDate;
  }
  

  settings() {
    this.navCtrl.push(SettingsPage);
  }

  goBack() {
    this.showMore = false;
    this.showLess = true;
  }

  async doRefresh(val) {
    this.showLess = false;
    this.showMore = false;
    setTimeout(() => {
      val.complete();
    }, 1000);
    let loginResponse = await this.store.fetchDoc('logindata');
    console.log(loginResponse);
    let deviceResponse = await this.store.fetchDoc('device');
    await this.fetchFromServer(loginResponse.sessionId, loginResponse.pencomPin, deviceResponse.uuid);
  }

  showMoreDetails() {
    this.showLess = false;
    this.showMore = true;
  }

}
