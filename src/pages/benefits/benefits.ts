import { Component } from '@angular/core';
import { NavController, Platform, App } from 'ionic-angular';
import { ServerServiceProvider } from '../../providers/server-service/server-service';
import { ControllerService } from'../../providers/controller-service';
import { DatePicker, DatePickerOptions } from '@ionic-native/date-picker';
import { BenefitFormPage } from '../benefit-form/benefit-form';
import { NativePageTransitions, NativeTransitionOptions } from '@ionic-native/native-page-transitions';

@Component({
  selector: 'page-benefits',
  templateUrl: 'benefits.html',
})
export class BenefitsPage {
  benefitType: any = [];
  errorMessage: string = '';
  paymentType: string = '';
  tin: string = '';

  dob: string = '';
  dod: string = '';
  _dob: string = '';
  _dod: string = '';

  options: NativeTransitionOptions = {
    direction: 'up',
    duration: 500,
    slowdownfactor: 3,
    slidePixels: 20,
    iosdelay: 100,
    androiddelay: 150,
    fixedPixelsTop: 0,
    fixedPixelsBottom: 60
   };

  monthName = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'];

  constructor(
    public navCtrl: NavController,
    private serverService: ServerServiceProvider,
    private controller: ControllerService,
    private datePicker: DatePicker,
    private platform: Platform,
    private app: App,
    private nativePageTransitions: NativePageTransitions
  ) {}

  ionViewDidLoad() {
    console.log('ionViewDidLoad BenefitsPage');
    this.getBenefitType();
  }

  ionViewDidEnter(){
    this.platform.registerBackButtonAction(() => {
      this.navCtrl.parent.select(0);
    });
  }

  async getBenefitType() {
    let loader = this.controller.showLoader('Loading...');
    loader.present();
    try {
      let serverResponse = await this.serverService.processData('', '/BenefitApplicationType');
      console.log(serverResponse);

      if (serverResponse.StatusCode == 'FF') {
        this.controller.showToast(serverResponse.Message, 'bottom');
        this.navCtrl.pop();
      }
      else if (serverResponse.StatusCode == '00') {
        for (let val of serverResponse.Data) {
          if (val.approvalIDField == 2 || val.approvalIDField == 7) {
            this.benefitType.push(val);
          }
        }
        console.log(this.benefitType);
      }
      else {
        this.errorMessage = 'Network error. Pull down to refresh page';
      }
    }
    catch(err) {
      console.log(err);
      this.errorMessage = 'Error occured. Pull down to refresh page';
    }
    loader.dismiss();
  }

  pickDate(dateType, selectedDate) {
    var pickedDate;
    if (selectedDate == '') {
      pickedDate = new Date();
    }
    else {
      pickedDate = (dateType == 'dob') ? new Date(this._dob) : new Date(this._dod);
    }

    let dateOptions: DatePickerOptions = {
      mode: "date",
      date: pickedDate,
      androidTheme: this.datePicker.ANDROID_THEMES.THEME_HOLO_LIGHT
    };

    this.datePicker.show(dateOptions).then((val) => {
      var dayDate = (val.getDate() < 10) ? '0'+val.getDate() : val.getDate().toString();
      var monthDate = this.monthName[val.getMonth()];
      var fullYear = val.getFullYear().toString();

      if (dateType == "dob") {
        if (this.getAge(val.toDateString()) > 49) {
          this.controller.showToast('Above 49years cannot apply for this benefit type', 'bottom');
          this.dob = '';
        } else if (this.getAge(val.toDateString()) < 18) {
          this.controller.showToast('Below 18years cannot apply for this benefit type', 'bottom');
          this.dob = '';
        }
        else {
          this.dob = dayDate + '/' + monthDate + '/' + fullYear;
          this._dob = val.toDateString();
        }
      }
      else {
        var dateDiff = Math.floor((Date.UTC(new Date().getFullYear(), new Date().getMonth(), new Date().getDate()) - Date.UTC(val.getFullYear(), val.getMonth(), val.getDate())) / (1000*60*60*24));
        if (dateDiff < 121) {
          this.controller.showToast('Date of disengagement cannot be less than 4 months', 'bottom');
          this.dod = '';
        }
        else {
          this.dod = dayDate + '/' + monthDate + '/' + fullYear;
          this._dod = val.toDateString();
        }
      }
    })
    .catch((err) => {
      return '';
    });
  }

  submit() {
    console.log(this.paymentType);
    if (this.paymentType == '7') {
      if (this.tin.trim() == '') {
        this.controller.showToast('Enter your Tax Identification Number (TIN)', 'bottom');
        return false;
      }
    }
    else {
      if (this.dob.trim() == '' || this.dod.trim() == '') {
        this.controller.showToast('All fields are required', 'bottom');
        return false;
      }
    }

    this.nativePageTransitions.slide(this.options);
    console.log(this.benefitType);
    let payName: string = '';

    for (let val of this.benefitType) {
      if (val.approvalIDField == Number.parseInt(this.paymentType)) {
        payName = val.approvalNameField;
        break;
      }
    }

    this.app.getRootNav().push(BenefitFormPage, {
      paymentType: this.paymentType,
      paymentName: payName,
      tin: this.tin
    });
  }

  getAge(dob) 
	{
	    var today = new Date();
	    var birthDate = new Date(dob);
	    var age = today.getFullYear() - birthDate.getFullYear();
	    var m = today.getMonth() - birthDate.getMonth();

	    if (m < 0 || (m === 0 && today.getDate() < birthDate.getDate())) {
	        age--;
      }
	    return age;
  }

}
