import { Component } from '@angular/core';
import { ViewController, NavParams } from 'ionic-angular';
import { ControllerService } from '../../providers/controller-service';
import { ServerServiceProvider } from '../../providers/server-service/server-service';

@Component({
  selector: 'page-bankbranch-modal',
  templateUrl: 'bankbranch-modal.html',
})
export class BankbranchModalPage {
  branchData: any = [];
  _branchData: any = [];

  constructor(
    public viewCtrl: ViewController, 
    public navParams: NavParams,
    private controller: ControllerService,
    private serverService: ServerServiceProvider
  ) {}

  ionViewDidLoad() {
    console.log('ionViewDidLoad BankbranchModalPage');
    this.getBankBranch(this.navParams.get('bankId'));
  }

  async getBankBranch(bankId) {
    let loader = this.controller.showLoader('Loading...');
    loader.present();
    try {
      let response = await this.serverService.processData('', '/GetBankBranches');
      console.log(response);

      if (response.StatusCode == '00') {
        var newData = [];
        newData = response.Data;

        for (let val of newData) {
          if (val.branchBankIDField == Number.parseInt(bankId)) {
            this.branchData.push(val);
            this._branchData.push(val);
          }
        }
      }
      else {
        this.controller.showToast('Error occurred. Try again later.', 'bottom');
        this.selectedBranch('Error');
      }
    }
    catch(err) {
      console.log(err);
      this.controller.showToast('Error occurred. Try again later.', 'bottom');
      this.selectedBranch('Error');
    }
    loader.dismiss();
  }

  selectedBranch(val) {
    this.viewCtrl.dismiss(val);
  }

  getItems(ev) {
    this.branchData = this._branchData 
    var val = ev.target.value;

    if (val && val.trim() != '') {
      this.branchData = this.branchData.filter((item) => {
        return (item.bankBranchNameField.toLowerCase().indexOf(val.toLowerCase()) > -1);
      });
    }
  }

}
