import { Component } from '@angular/core';
import { NavController, Platform } from 'ionic-angular';
import { CalculatorPage } from '../calculator/calculator';
import { ProductsPage } from '../products/products';
import { FaqPage } from '../faq/faq';
import { ChartPage } from '../chart/chart';
import { BranchesPage } from '../branches/branches';
import { SurveyPage } from '../survey/survey';

@Component({
  selector: 'page-home',
  templateUrl: 'home.html',
})
export class HomePage {
  image: string = '';
  fullName: string = '';

  constructor(
    public navCtrl: NavController,
    private platform: Platform
  ) {}

  ionViewDidLoad() {
    console.log('ionViewDidLoad HomePage');
  }

  ionViewDidEnter(){
    this.platform.registerBackButtonAction(() => {
      this.navCtrl.parent.select(0);
    });
  }

  survey() {
    this.navCtrl.push(SurveyPage);
  }

  benefit() {
    this.navCtrl.push(ProductsPage);
  }

  chart() {
    this.navCtrl.push(ChartPage);
  }

  calculator() {
    this.navCtrl.push(CalculatorPage);
  }

  branches() {
    this.navCtrl.push(BranchesPage);
  }

  faq() {
    this.navCtrl.push(FaqPage);
  }

}
