import { Component } from '@angular/core';
import { NavController, Platform, NavParams } from 'ionic-angular';
import { StorageService } from '../../providers/storage-service';
import { ServerServiceProvider } from '../../providers/server-service/server-service';
import { ControllerService } from '../../providers/controller-service';
import { NativeService } from '../../providers/native-service';
import { IonDigitKeyboard, IonDigitKeyboardOptions } from '../../components/ion-digit-keyboard/ion-digit-keyboard';
import { TabPage } from '../tab/tab';
import { HelpPage } from '../help/help';
import { TouchID } from '@ionic-native/touch-id';

@Component({
  selector: 'page-login',
  templateUrl: 'login.html',
})
export class LoginPage {
  mobilePin: string;
  private serverData: any;
  input1: string = "";
  input2: string = "";
  input3: string = "";
  input4: string = "";
  imageURL: string = "assets/images/user.png";
  userName: string = '';

  keyboardSettings: IonDigitKeyboardOptions = {
        align: 'center',
        width: '95%',
        visible: true,
        leftActionOptions: {
            iconName: 'backspace',
            fontSize: '1.2em'
        },
        roundButtons: false,
        showLetters: false,
        swipeToHide: true,
        theme: 'opaque-white'
    };

  constructor(
    public navCtrl: NavController,
    private store: StorageService,
    private serverService: ServerServiceProvider,
    private controller: ControllerService,
    private native: NativeService,
    private touchId: TouchID,
    private platform: Platform,
    private navParam: NavParams
  ) {}

  ionViewDidLoad() {
    console.log('ionViewDidLoad LoginPage');

    if (this.navParam.get('timedout')) {
      this.controller.showToast('Session timeout. Please re-login', 'middle');
    }

    if (this.platform.is('android')) {
      this.scanFingerprint();
    }
    else if (this.platform.is('ios')) {
      this.iOSTouchID();
    }
    this.getImage();
  }

  iOSTouchID() {
    this.touchId.isAvailable().then(res => {
      this.touchId.verifyFingerprint('Scan your fingerprint please').then(res => {
        this.login('print');
      }, err => {
        console.log(err);
      });
    },
    err => {
      console.log(err);
    });
  }

  async scanFingerprint() {
    let response = await this.native.scanFingerprint();
    
    if (response != "Failed" && response != "Cancel") {
      this.login("print");
    }
  }

  onKeyboardButtonClick(key: any) {
    console.log(key);
    if (key != "right" && key != "left") {
      if (this.input1 == "") {
        this.input1 = key.toString();
      } 
      else if (this.input2 == "") {
        this.input2 = key.toString();
      }
      else if (this.input3 == "") {
        this.input3 = key.toString();
      }
      else if (this.input4 == "") {
        this.input4 = key.toString();
        this.mobilePin = this.input1 + this.input2 + this.input3 + this.input4;
        this.login("pin");
      }
    }

    if (key == "left") {
      if (this.input4 != "") {
        this.input4 = "";
      } else if (this.input3 != "") {
        this.input3 = "";
      } else if (this.input2 != "") {
        this.input2 = "";
      } else if (this.input1 != "") {
        this.input1 = "";
      }
    }
  }

  async login(loginType) {
    let loadCtrl = this.controller.showLoader('Signing in...');
    loadCtrl.present();

    const deviceResponse = await this.store.fetchDoc('device');
    const loginResponse = await this.store.fetchDoc('logindata');
    console.log(loginResponse);
    console.log(deviceResponse);

    if (loginType == "print") {
      this.serverData = {
        PencomPIN: loginResponse.pencomPin,
        deviceId: deviceResponse.uuid,
        MobilePin: "",
        fingerprint: true
      };
    } else {
      this.serverData = {
        PencomPIN: loginResponse.pencomPin,
        deviceId: deviceResponse.uuid,
        MobilePin: this.mobilePin,
        fingerprint: false
      };
    }
    var funcName = '/LoginUser';

    try {
      let serverResponse = await this.serverService.processData(this.serverData, funcName);
      console.log(serverResponse);

      if (serverResponse.StatusCode == "FF") {
        this.clearInputs();
        this.controller.showAlert(serverResponse.Message);
      }
      else if (serverResponse.StatusCode == "00") {
        loginResponse.sessionId = serverResponse.Data.sessionid;
        loginResponse.email = serverResponse.Data.email;

        let storageResponse = await this.store.createUpdateDoc(loginResponse);
        console.log(storageResponse);

        this.navCtrl.setRoot(TabPage);
      }
      else {
        this.controller.showToast('Unable to login. Check your connection', 'bottom');
        this.clearInputs();
      }
    }
    catch(err) {
      console.log(err);
      this.controller.showToast('Unable to login. Check your connection', 'bottom');
      this.clearInputs();
    }
    loadCtrl.dismiss();
  }

  async getImage() {
    let imageResponse = await this .store.fetchDoc('biometric');
    let userResponse = await this.store.fetchDoc('user');
    
    if (imageResponse != 'Failed' && imageResponse.image != '') {
      this.imageURL = imageResponse.image;
    }

    if (userResponse != 'Failed') {
      this.userName = userResponse.firstName + ' ' + userResponse.lastName;
    }
  }

  clearInputs(): void {
    this.input1 = "";
    this.input2 = "";
    this.input3 = "";
    this.input4 = "";
  }

  getHelp() {
    this.navCtrl.push(HelpPage);
  }

}
