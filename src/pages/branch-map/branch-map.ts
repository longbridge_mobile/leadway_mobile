import { Component } from '@angular/core';
import { NavController, NavParams, Platform } from 'ionic-angular';
declare var google;

@Component({
  selector: 'page-branch-map',
  templateUrl: 'branch-map.html',
})
export class BranchMapPage {
  branchName: string = '';

  constructor(
    public navCtrl: NavController, 
    public navParams: NavParams,
    private platform: Platform
  ) {}

  ionViewDidLoad() {
    console.log('ionViewDidLoad BranchMapPage');
    this.branchName = this.navParams.get('branchName');
  }
  ionViewDidEnter(){
    this.platform.registerBackButtonAction(() => {
      this.leavePage();
    });
  }

  ngAfterViewInit() {
    this.loadMap();
  }

  loadMap() {
    let latLong = {
      lat: Number(this.navParams.get('lat')),
      lng: Number(this.navParams.get('long'))
    };

    let map = new google.maps.Map(document.getElementById('map'), {
      zoom: 10,
      center: latLong
    });

    let marker = new google.maps.Marker({
      position: latLong,
      map: map,
      label: 'LP',
      title: 'Leadway Pensure'
    });
  }

  leavePage() {
    this.navCtrl.pop();
  }

}
