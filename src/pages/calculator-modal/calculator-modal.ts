import { Component } from '@angular/core';
import { NavController, NavParams, ViewController, Platform } from 'ionic-angular';
import { PopoverController, PopoverOptions } from 'ionic-angular';
import { CalculatorPopPage } from '../calculator-pop/calculator-pop';

@Component({
  selector: 'page-calculator-modal',
  templateUrl: 'calculator-modal.html',
})
export class CalculatorModalPage {
  pension: boolean = false;
  retirement: boolean = false;
  disableButton: boolean = true;

  monthlyCollection: string = "";
  retirementNum: string = "";
  pensionReturns: string = "";
  tenor: string = "";

  openBalance: string = "";
  monthlyContribution: string = "";
  yearsToRetire: string = "";
  expectedReturn: string = "";
  collectionPeriod: string = "";

  constructor(
    private navParam: NavParams,
    private popCtrl: PopoverController,
    private viewCtrl: ViewController,
    private navCtrl: NavController,
    private platform: Platform
  ) {}

  ionViewDidLoad() {
    console.log('ionViewDidLoad CalulatorModalPage');
    this.pension = this.navParam.get('pension');
    this.retirement = this.navParam.get('retire');
  }

  ionViewDidEnter(){
    this.platform.registerBackButtonAction(() => {
      this.close();
    });
  }

  private enableButton() {
    if (this.pension) {
      if (this.monthlyCollection.trim() != "" && this.retirementNum.trim() != "" && this.pensionReturns.trim() != "" && this.tenor.trim() != "") {
        this.disableButton = false;
      }
      else {
        this.disableButton = true;
      }
    }
    else if (this.retirement) {
      if (this.openBalance.trim() != "" && this.monthlyContribution.trim() != "" && this.yearsToRetire.trim() != "" && this.expectedReturn.trim() != "" && this.collectionPeriod.trim() != "") {
        this.disableButton = false;
      }
      else {
        this.disableButton = true;
      }
    }
  }

  calculate() {
    if (this.pension && !this.retirement) {
      this.desiredPension();
    }
    else if (!this.pension && this.retirement) {
      this.retirementCollection();
    }
  }

  private desiredPension() {
    var contrmonth = Number(this.retirementNum) * 12;
    var rtt = Number(this.pensionReturns) / 100;
    var ratereturn = rtt / 12;
    // var tpensure = this.tenor * 12;
    var years = 12;
    var pow1 = Math.pow((1 + ratereturn), years);
    var pow2 = Math.pow((1 + rtt), Number(this.tenor));
    var pow3 = Math.pow((1 + ratereturn), contrmonth)
    var bigC = Number(this.monthlyCollection) * ((1 / ratereturn) - (1 / (ratereturn * pow1)));
    var fv = bigC * ((1 / rtt) - (1 / (rtt * pow2)));
    var pmt = fv / ((pow3 - 1) / ratereturn);

    var RSA = Number(fv.toFixed(2)).toLocaleString();
    var monthlyCont = Number(pmt.toFixed(2)).toLocaleString();

    this.showPopover("RSA Required", "Amount you need to contribute monthly", RSA, monthlyCont);
  }

  private retirementCollection() {
    var N = Number(this.yearsToRetire) * 12;
    var rtt1 = Number(this.expectedReturn) / 100;
    var rateret2 = rtt1 / 12;
    var smallt = 1;
    var pow1 = Math.pow((1 + rateret2), smallt);
    var pow2 = Math.pow((1 + rateret2), N);
    var pow3 = Math.pow((1 + rtt1), Number(this.collectionPeriod));
    var ans = (1 / rateret2) - (1 / (rateret2 * pow1));
    var fv1 = (Number(this.openBalance) * pow2) + (Number(this.monthlyContribution) * ((pow2 - 1) / rateret2));
    var C1 = fv1 / ((1 / rtt1) - (1 / (rtt1 * pow3)));
    var tc = C1 / ans;
    var contr = tc / 12;

    var closingRSA = Number(fv1.toFixed(2)).toLocaleString();
    var monthlyPmt = Number(contr.toFixed(2)).toLocaleString();

    this.showPopover("Closing RSA", "Amount to be collected per month", closingRSA, monthlyPmt);
  }

  private showPopover(labelName1: string, labelName2: string, value1: string, value2: string) {
    let popOptions: PopoverOptions = {
      enableBackdropDismiss: false,
      // showBackdrop: true
    };

    let data = {
      labelName1: labelName1,
      labelName2: labelName2,
      value1: value1,
      value2: value2
    };
    this.popCtrl.create(CalculatorPopPage, data, popOptions).present();
  }

  close() {
    if (CalculatorModalPage) {
      this.viewCtrl.dismiss();
    }
    else {
      this.navCtrl.pop();
    }
  }

}
