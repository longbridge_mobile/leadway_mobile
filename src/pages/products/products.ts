import { Component } from '@angular/core';
import { Platform, NavController } from 'ionic-angular';
import { ServerServiceProvider } from '../../providers/server-service/server-service';
import { ControllerService } from '../../providers/controller-service';
import { FaqModalPage } from '../faq-modal/faq-modal';

@Component({
  selector: 'page-products',
  templateUrl: 'products.html',
})
export class ProductsPage {
  productData: any = [];

  constructor(
    private serverService: ServerServiceProvider,
    private controller: ControllerService,
    private platform: Platform,
    private navCtrl: NavController
  ) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad ProductsPage');
    this.getProductList();
  }

  ionViewDidEnter(){
    this.platform.registerBackButtonAction(() => {
      this.leavePage();
    });
  }

  async getProductList() {
    let loader = this.controller.showLoader("Getting products...");
    loader.present();

    try {
      let serverResponse = await this.serverService.processData('', '/ProductList');
      console.log(serverResponse);

      if (serverResponse.StatusCode == "FF") {
        this.controller.showToast(serverResponse.Message, 'bottom');
      }
      else if (serverResponse.StatusCode == "00") {
        this.productData = serverResponse.Data;
      }
    }
    catch(err) {
      console.log(err);
      this.controller.showToast("Unable to get product list. Try again later", "bottom");
    }
    loader.dismiss();
  }

  viewProductDetails(selectedProduct) {
    let data = {
      name: selectedProduct.ProductHeader,
      details: selectedProduct.productDetails,
      title: 'Product Details'
    };
    this.controller.showModal(FaqModalPage, data);
  }

  doRefresh(val) {
    this.productData = [];
    setTimeout(() => {
      val.complete();
    }, 1000);
    this.getProductList();
  }

  leavePage() {
    this.navCtrl.pop();
  }

}
