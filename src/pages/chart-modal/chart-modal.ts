import { Component, ViewChild } from '@angular/core';
import { NavParams, ViewController, NavController, Platform } from 'ionic-angular';
import Chart from 'chart.js';

@Component({
  selector: 'page-chart-modal',
  templateUrl: 'chart-modal.html',
})
export class ChartModalPage {
  @ViewChild('chartCanvas') chartCanvas;
  drawChart: any = "";
  _labelData: any;
  _valueData: any;
  chartType: string = "";

  constructor(
    private viewCtrl: ViewController,
    private navParam: NavParams,
    private navCtrl: NavController,
    private platform: Platform
  ) {}

  ionViewDidLoad() {
    console.log('ionViewDidLoad ChartModalPage');
    this._labelData = this.navParam.get('label');
    this._valueData = this.navParam.get('value');
    this.chartType = this.navParam.get('chartType');
    this.changeChartType(this.navParam.get('chartType'));
  }

  ionViewDidEnter(){
    this.platform.registerBackButtonAction(() => {
      this.close();
    });
  }

  swipeEvent(val) {
    if (val.direction == 2) {
      if (this.chartType == "lineChart") {
        this.chartType = "barChart";
        this.changeChartType("barChart");
      }
    }
    else if (val.direction == 4) {
      if (this.chartType == "barChart") {
        this.chartType = "lineChart";
        this.changeChartType("lineChart");
      }
    }
  }

  changeChartType(chartType) {
    if (this.drawChart != "") {
      this.drawChart.destroy();
    }
    
    if (chartType == 'lineChart') {
      this.drawLineChart(this._labelData, this._valueData);
    }
    else {
      this.drawBarChart(this._labelData, this._valueData);
    }
  }

  drawLineChart(labelData, valueData) {
    this.drawChart = new Chart(this.chartCanvas.nativeElement, {
      type: 'line',
      data: {
        labels: labelData,
        datasets: [
          {
            label: 'Price Chart',
            fill: true,
            lineTension: 0.3,
            backgroundColor: 'rgba(93,93,93,0.6)',
            borderColor: 'rgb(93,93,93)',
            borderCapStyle: 'butt',
            borderDash: [],
            borderDashOffset: 0.0,
            borderJoinStyle: 'miter',
            pointBorderColor: '#fff',
            pointBackgroundColor: 'rgb(248,130,44)',
            pointBorderWidth: 1,
            pointHoverRadius: 2,
            pointHoverBackgroundColor: 'rgb(93,93,93)',
            pointHoverBorderColor: 'rgba(220,220,220,1)',
            pointHoverBorderWidth: 2,
            pointRadius: 4,
            data: valueData,
            spanGaps: false
          }
        ]
      },
      options: {
        scales: {
          yAxes:[{
            ticks: {
              beginAtZero: false
            }
          }]
        }
      }
    });
  }

  drawBarChart(labelData, valueData) {
    this.drawChart = new Chart(this.chartCanvas.nativeElement, {
      type: 'bar',
      data: {
        labels: labelData,
        datasets: [
          {
            label: 'Price Chart',
            backgroundColor: 'rgba(93,93,93,0.6)',
            borderColor: 'rgb(93,93,93)',
            borderWidth: 1,
            data: valueData
          }
        ]
      },
      options: {
        scales: {
          yAxes: [{
            ticks: {
              beginAtZero: false
            }
          }]
        }
      }
    });
  }

  close() {
    if (ChartModalPage) {
      this.viewCtrl.dismiss();
    }
    else {
      this.navCtrl.pop();
    }
  }

}
