import { Component } from '@angular/core';
import { NavController, Platform } from 'ionic-angular';
import { ControllerService } from '../../providers/controller-service';
import { StorageService } from '../../providers/storage-service';
import { ServerServiceProvider } from '../../providers/server-service/server-service';
import { ViewMessagePage } from '../view-message/view-message';

@Component({
  selector: 'page-notification',
  templateUrl: 'notification.html',
})
export class NotificationPage {
  notificationMsg: string = "";
  notificationData: any = [];

  constructor(
    public navCtrl: NavController,
    private controller: ControllerService,
    private store: StorageService,
    private serverService: ServerServiceProvider,
    private platform: Platform
  ) {}

  ionViewDidLoad() {
    console.log('ionViewDidLoad NotificationPage');
    this.getMessages();
  }

  ionViewDidEnter() {
    this.platform.registerBackButtonAction(() => {
      this.navCtrl.parent.select(0);
    });
  }

  async getMessages() {
    let loader = this.controller.showLoader("Getting your messages...");
    loader.present();

    let loginResponse = await this.store.fetchDoc('logindata');
    let data = {
      PencomPin: loginResponse.pencomPin
    };
    var _funcName = '/GetPersonalNotification';
    var funcName = "/getNewNotification";

    try {
      let serverResponse = await this.serverService.processData('', funcName);
      let response = await this.serverService.processData(data, _funcName);
      console.log(serverResponse);
      console.log(response);

      if (serverResponse.StatusCode == "00") {
        for (let val of serverResponse.Data) {
          this.notificationData.push({
            title: val.MessageTitle,
            message: val.MessageNotification,
            dateAdded: val.DateAdded
          });
        }
      }

      if (response.StatusCode == "00") {
        for (let val of response.Data) {
          this.notificationData.push({
            title: val.PersonalMessageTitle,
            message: val.PersonalMessageNotification,
            dateAdded: val.DateAdded
          });
        }
      }

      if (serverResponse.StatusCode == "FF" && response.StatusCode == "FF") {
        this.notificationMsg = serverResponse.Message;
      }
    }
    catch(err) {
      console.log(err);
      this.notificationData = "No notification";
    }
    loader.dismiss();
  }

  async viewMessage(msg) {
    this.navCtrl.push(ViewMessagePage, {
      message: msg.message,
      title: msg.title
    });
  }

  doRefresh(val) {
    this.notificationData = [];
    this.notificationMsg = ""
    val.complete();
    this.getMessages();
  }

}
