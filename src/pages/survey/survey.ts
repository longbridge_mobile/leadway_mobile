import { Component } from '@angular/core';
import { NavController, Platform } from 'ionic-angular';
import { StorageService } from '../../providers/storage-service';
import { ControllerService } from '../../providers/controller-service';
import { ServerServiceProvider } from '../../providers/server-service/server-service';

@Component({
  selector: 'page-survey',
  templateUrl: 'survey.html',
})
export class SurveyPage {
  surveyData: any = [];
  answerSurvey: any;
  questionArray: any = [];
  answerArray: any = [];
  surveyCheck: any = [];

  constructor(
    public navCtrl: NavController,
    private platform: Platform,
    private serverService: ServerServiceProvider,
    private store: StorageService,
    private controller: ControllerService
  ) {}

  ionViewDidLoad() {
    console.log('ionViewDidLoad SurveyPage');
    this.getSurvey();
  }

  ionViewDidEnter(){
   this.platform.registerBackButtonAction(() => {
    this.leavePage();
   });
  }

  async getSurvey() {
    let loader = this.controller.showLoader('Loading...');
    loader.present();

    let surveyResp = await this.store.fetchDoc('survey');
    console.log(surveyResp);

    let loginResponse = await this.store.fetchDoc('logindata');
    let deviceResponse = await this.store.fetchDoc('device');

    let data = {
      pencomPin: loginResponse.pencomPin,
      mobileId: deviceResponse.uuid
    };
    var funcName = '/SurveyQuestions';

    if (surveyResp != 'Failed' && surveyResp.list != []) {
      this.surveyCheck = surveyResp.list;
    }

    try {
      let response = await this.serverService.processData(data, funcName);

      if (response.StatusCode == '00') {
        for (let val of this.surveyCheck) {
          if (val.surveyGroup = response.Data[0].surveyGroupId) {
            this.controller.showToast('No survey available at this moment', 'bottom');
            this.leavePage();
            break;
          }
        }
        this.surveyData = response.Data;
      }
      else if (response.StatusCode == 'FF') {
        this.controller.showToast(response.Message, 'bottom');
        this.leavePage();
      }
      else {
        this.controller.showToast('Network error. Try again later.', 'bottom');
        this.leavePage();
      }
    }
    catch(err) {
      console.log(err);
      this.controller.showToast('An error occured.', 'bottom');
      this.leavePage();
    }
    loader.dismiss();

  }

  surveyAnswer(ev, val, index) {
    this.questionArray[index] = val.Question;
    this.answerArray[index] = ev;
  }

  async submitSurvey() {
    if (this.questionArray.length < this.surveyData.length) {
      this.controller.showToast("Kindly answer all the survey questions.", 'bottom');
      return false;
    }
    let loader = this.controller.showLoader('Submitting survey...');
    loader.present();

    let loginResponse = await this.store.fetchDoc('logindata');

    let data = {
      "surveyGroupId": this.surveyData[0].surveyGroupId,
      "Question": this.questionArray,
      "selectedAnswer": this.answerArray,
      "pencomPin": loginResponse.pencomPin
    }
    var funcName = '/submitSurvey';

    try {
      let serverResponse = await this.serverService.processData(data, funcName);

      if (serverResponse.StatusCode == '00') {
        this.controller.showToast(serverResponse.Message, 'bottom');
        this.surveyDoc();
        this.leavePage();
      }
      else if (serverResponse.StatusCode == 'FF') {
        this.controller.showToast(serverResponse.Message, 'bottom');
      }
      else {
        this.controller.showToast('An error occured while submitting. Try again later.', 'bottom');
      }
    }
    catch(err) {
      console.log(err);
      this.controller.showToast('An error occured while submitting. Try again later.', 'bottom');
    }
    loader.dismiss();
  }

  doRefresh(val) {
    this.surveyData = [];
    setTimeout(() => {
      val.complete();
    }, 1000);
    this.getSurvey();
  }

  async surveyDoc() {
    let surveyResp = await this.store.fetchDoc('survey');

    if (surveyResp == 'Failed') {
      let survey = {
        _id: 'survey',
        list: [
          {surveyGroup: this.surveyData[0].surveyGroupId}
        ]
      };
      const resp = await this.store.createUpdateDoc(survey);
      console.log(resp);
    }
    else {
      surveyResp.list.push({surveyGroup: this.surveyData[0].surveyGroupId});
      const resp = await this.store.createUpdateDoc(surveyResp);
      console.log(resp);
    }
  }

  leavePage() {
    this.navCtrl.pop();
  }

}
