import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import { IonDigitKeyboard, IonDigitKeyboardOptions } from '../../components/ion-digit-keyboard/ion-digit-keyboard';
import { LoginPage } from '../login/login';
import { ControllerService } from '../../providers/controller-service';
import { ServerServiceProvider } from '../../providers/server-service/server-service';
import { StorageService } from '../../providers/storage-service';

@Component({
  selector: 'page-confirm-new-pin',
  templateUrl: 'confirm-new-pin.html',
})
export class ConfirmNewPinPage {
  keyboardSettings: IonDigitKeyboardOptions = {
        align: 'center',
        width: '95%',
        visible: true,
        leftActionOptions: {
            iconName: 'backspace',
            fontSize: '1.2em'
        },
        rightActionOptions: {
            iconName: 'checkmark-circle',
            // text: 'Enter',
            fontSize: '1.2em'
        },
        roundButtons: false,
        showLetters: false,
        swipeToHide: true,
        theme: 'opaque-white'
    };

    input1: string = "";
    input2: string = "";
    input3: string = "";
    input4: string = "";
    mobilePin: string = "";
    newPin: string = "";
    otp: string = "";

  constructor(
    public navCtrl: NavController, 
    private navParams: NavParams,
    private controller: ControllerService,
    private serverService: ServerServiceProvider,
    private store: StorageService
  ) {}

  ionViewDidLoad() {
    console.log('ionViewDidLoad ConfirmNewPinPage');
    this.newPin = this.navParams.get("newPin");
    this.otp = this.navParams.get("otp");
  }

  onKeyboardButtonClick(key: any) {
    console.log(key);
    if (key != "right" && key != "left") {
      if (this.input1 == "") {
        this.input1 = key.toString();
      } 
      else if (this.input2 == "") {
        this.input2 = key.toString();
      }
      else if (this.input3 == "") {
        this.input3 = key.toString();
      }
      else if (this.input4 == "") {
        this.input4 = key.toString();
      }
    }

    if (key == "left") {
      if (this.input4 != "") {
        this.input4 = "";
      } else if (this.input3 != "") {
        this.input3 = "";
      } else if (this.input2 != "") {
        this.input2 = "";
      } else if (this.input1 != "") {
        this.input1 = "";
      }
    }

    if (key == "right") {
      this.mobilePin = this.input1 + this.input2 + this.input3 + this.input4;
      console.log(this.mobilePin);

      if (this.mobilePin.length < 4) {
        this.controller.showToast("Mobile pin cannot be less than 4 digits", "bottom");
      } else if (this.mobilePin != this.newPin) {
        this.controller.showToast("Mobile pins do not match", "bottom");
      } else {
        if (this.otp == 'change') {
          this.changePin();
        }
        else {
          if (this.navParams.get('OTPType') == 0) {
            this.registerUser();
          }
          else {
            this.resetPin();
          }
        }
      }
    }
  }

  async registerUser() {
    let loader = this.controller.showLoader("Please wait...");
    loader.present();

    let deviceResponse = await this.store.fetchDoc("device");
    let loginResponse = await this.store.fetchDoc("logindata");

    let data = {
      deviceId: deviceResponse.uuid,
      pencomPin: loginResponse.pencomPin,
      mobilePin: this.mobilePin
    };
    var funcName = "/RegisterUser";

    try {
      let serverResponse = await this.serverService.processData(data, funcName);
      console.log(serverResponse);

      if (serverResponse.StatusCode == "FF") {
          this.controller.showToast(serverResponse.Message, "bottom");
        }
        else if (serverResponse.StatusCode == "00") {
          this.controller.showToast(serverResponse.Message, "bottom");
          this.navCtrl.setRoot(LoginPage, {
            timedout: false
          });
        }
    }
    catch(err) {
      console.log(err);
      this.controller.showToast("Network error. Try again", "bottom");
    }
    loader.dismiss();
  }

  async resetPin() {
    let loader = this.controller.showLoader('Resetting pin...');
    loader.present();

    let loginResponse = await this.store.fetchDoc("logindata");

    let data = {
      pencomPin: loginResponse.pencomPin,
      mobilePin: this.mobilePin,
      OTP: this.otp
    };
    var funcName = '/ForgotMobilePin';

    try {
      let response = await this.serverService.processData(data, funcName);
      console.log(response);

      if (response.StatusCode == '00') {
        this.controller.showToast(response.Message, "bottom");
        this.navCtrl.setRoot(LoginPage, {
          timedout: false
        });
      }
      else if (response.StatusCode == 'FF') {
        this.controller.showToast(response.Message, "bottom");
      }
      else {
        this.controller.showToast('Network error. Check your network connection.', 'bottom');
      }
    }
    catch(err) {
      console.log(err);
      this.controller.showToast('Error occurred. Try again later.', 'bottom');
    }
    loader.dismiss();
  }

  async changePin() {
    let loader = this.controller.showLoader('Changing pin...');
    loader.present();

    let login = await this.store.fetchDoc('logindata');
    let device = await this.store.fetchDoc('device');

    let data = {
      deviceId: device.uuid,
      pencomPin: login.pencomPin,
      oldMobilePin: this.navParams.get('oldPin'),
      newMobilePin: this.newPin
    };
    var funcName = '/ChangePassword';

    try {
      let response = await this.serverService.processData(data, funcName);
      console.log(response);

      if (response.StatusCode == '00') {
        this.controller.showToast(response.Message + '. Login with your new mobile pin.', 'bottom');
        this.navCtrl.setRoot(LoginPage, {
          timedout: false
        });
      }
      else if (response.StatusCode == 'FF') {
        this.controller.showToast(response.Message, 'bottom');
      }
      else {
        this.controller.showToast('Network error. Check your network connection.', 'bottom');
      }
    }
    catch(err) {
      console.log(err);
      this.controller.showToast('Error occurred. Try again later.', 'bottom');
    }
    loader.dismiss();
  }

  close() {
    this.navCtrl.pop();
  }

}
