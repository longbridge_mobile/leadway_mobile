import { Component } from '@angular/core';
import { NavController, Platform } from 'ionic-angular';
import { ServerServiceProvider } from '../../providers/server-service/server-service';
import { StorageService } from '../../providers/storage-service';
import { ControllerService } from '../../providers/controller-service';
import { SearchTransactionPage } from '../search-transaction/search-transaction';
import { TranDetailPage } from '../tran-detail/tran-detail';

@Component({
  selector: 'page-account',
  templateUrl: 'account.html',
})
export class AccountPage {
  homeDetails: any = 'accountSummary';
  serverResponse: any = null;
  showPage: boolean = false;

  pencomPin: string = '';
  unitPriceDate: string = '';
  unitPrice: string = '';
  lastValueDate: string = '';
  totalContribution: string = '';
  investedIncome: string = '';
  fundExited: string = '';
  contibutionValue: string = '';
  avcInvestmentIncome: string = '';
  avcUnit: string = '';
  avcValue: string = '';
  totalUnit: string = '';
  fundType: string = '';

  transactionData: any = [];
  transactionsField: any = [];

  constructor(
    public navCtrl: NavController,
    public platform: Platform,
    private serverService: ServerServiceProvider,
    private store: StorageService,
    private controller: ControllerService
  ) {}

  ionViewDidLoad() {
    console.log('ionViewDidLoad AccountPage');
    this.getAccountSummary();
  }

  ionViewDidEnter(){
   this.platform.registerBackButtonAction(() => {
      this.controller.closeAppAlert();
    });
  }

  async getAccountSummary() {
    let loader = this.controller.showLoader('Loading...');
    loader.present();

    let loginResponse = await this.store.fetchDoc("logindata");

    let data = {
      sessionID: loginResponse.sessionId
    };

    let data2 = {
      sessionID: loginResponse.sessionId,
      endDate: "",
      startDate: "",
      RSAFund: true,
      RetireeFund: true,
      GuinessGratuityFund: true
    };

    var funcName1 = "/SummaryAccountStatement";
    var funcName2 = "/DetailedAccountStatement";

    let response = await this.serverCall(data, funcName1);
    let response1 = await this.serverCall(data2, funcName2);

    if (response != '') {
      if (response.Data != null) {
        this.pencomPin = response.Data[0].pinField;
        this.unitPriceDate = response.Data[0].unitpricedateField;
        this.unitPrice = '₦' + response.Data[0].unitpriceField;
        this.lastValueDate = response.Data[0].lastvaluedateField;
        this.totalContribution = '₦' + response.Data[0].totalamountcontributedField;
        this.fundExited = response.Data[0].fundsexitedField;
        this.contibutionValue = '₦' + response.Data[0].valueofcontributionField;
        this.showPage = true;
      }
    }

    try {
      if (response1.Data != null) {
        this.fundType = response1.Data.FundType;
        this.avcInvestmentIncome = '₦' + response1.Data.funds_account_details.summaryField.investmentincomeonavcField;
        this.avcUnit = response1.Data.funds_account_details.summaryField.avcunitField;
        this.totalUnit = response1.Data.funds_account_details.summaryField.totalunitsField;
        this.investedIncome = '₦' + response1.Data.funds_account_details.summaryField.investmentincometotalField;
        this.transactionsField = response1.Data.funds_account_details.transactionsField.reverse();
        this.avcValue = '₦' + response1.Data.funds_account_details.summaryField.valueonavcField;

        for (var i=0; i<this.transactionsField.length; i++) {
          if (i == 6) {
            break;
          }
          this.transactionData.push(this.transactionsField[i]);
        }

        this.saveAVCValue(response1.Data.funds_account_details.summaryField.valueonavcField);
      }
    }
    catch (err) {
      console.log(err);
    }
    loader.dismiss();
  }

  async mailAccountSummary() {
    this.controller.showToast('Requesting...', 'bottom');

    let loginResponse = await this.store.fetchDoc("logindata");

    let data = {
      sessionID: loginResponse.sessionId,
      endDate: "",
      startDate: "",
      RSAFund: true,
      RetireeFund: true,
      GuinessGratuityFund: true
    };
    var funcName = "/MailAccountSummary";

    let response = await this.serverCall(data, funcName);
    if (response.StatusCode == '00') {
      this.controller.showToast('Request sent', 'bottom');
    }
  }

  swipeEvent(val) {
    if (val.direction == 2 && this.homeDetails == "accountSummary") {
      this.homeDetails = "tranDetails";
    }
    else if (val.direction == 4 && this.homeDetails == "tranDetails") {
      this.homeDetails = "accountSummary";
    }
  }

  async serverCall(data, funcName) {
    try {
      this.serverResponse = await this.serverService.processData(data, funcName);

      if (this.serverResponse.StatusCode == "00") {
        return this.serverResponse;
      }
      else if (this.serverResponse.StatusCode == "FF") {
        this.controller.showToast(this.serverResponse.Message, 'bottom');
        return '';
      }
      else {
        this.controller.showToast("Error encountered. Try again.", "bottom");
        return '';
      }
    }
    catch(err) {
      console.log(err);
      this.controller.showToast("Error encountered. Try again.", "bottom");
      return '';
    }
  }

  tranDetails(val) {
    this.navCtrl.push(TranDetailPage, {
      tranData: val
    });
  }

  listTransactions() {
    this.navCtrl.push(SearchTransactionPage);
  }

  async saveAVCValue(avc) {
    let userResponse = await this.store.fetchDoc('user');
    console.log(userResponse);
    
    try {
      userResponse.avcValue = avc;
      await this.store.createUpdateDoc(userResponse);
    }
    catch(err) {
      console.log(err);
    }
  }

}
