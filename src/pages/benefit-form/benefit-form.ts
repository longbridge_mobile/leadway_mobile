import { Component } from '@angular/core';
import { NavController, NavParams, Platform } from 'ionic-angular';
import { ControllerService } from '../../providers/controller-service';
import { StorageService } from '../../providers/storage-service';
import { NativeService } from '../../providers/native-service';
import { ServerServiceProvider } from '../../providers/server-service/server-service';
import { AppDataServiceProvider } from '../../providers/app-data-service/app-data-service';
import { BranchModalPage } from '../branch-modal/branch-modal';
import { BankbranchModalPage } from '../bankbranch-modal/bankbranch-modal';
import { AlertController, AlertOptions} from 'ionic-angular';
import { PopoverController, PopoverOptions} from 'ionic-angular';
import { SignaturePadPage } from '../signature-pad/signature-pad';
import { DomSanitizer } from '@angular/platform-browser';
import { ActionSheetController, ActionSheetOptions } from 'ionic-angular';

@Component({
  selector: 'page-benefit-form',
  templateUrl: 'benefit-form.html',
})
export class BenefitFormPage {
  paymentType: string = '';
  taxId: string = '';
  designation: string = '';
  department: string = '';
  disengageRsn: string = '';
  acctName: string = '';
  acctNum: string = '';
  bvn: string = '';
  amount: number = null;
  bank: string = '';
  bankId: string = '';
  bankBranch: string = '';
  bankBranchId: string = '';
  image: string = '';
  avcValue: number = 0;

  passport: string = 'assets/images/add_image.png';

  disengageData: any = [];
  appDocData: any = [];
  documentData: any = [];
  _documentData: any = [];

  pencomPin: string = '';

  constructor(
    public navCtrl: NavController, 
    public navParams: NavParams,
    private platform: Platform,
    private store: StorageService,
    private controller: ControllerService,
    private serverService: ServerServiceProvider,
    private native: NativeService,
    private alertCtrl: AlertController,
    private popCtrl: PopoverController,
    private appServer: AppDataServiceProvider,
    private sanitize: DomSanitizer,
    private actionSheet: ActionSheetController
  ) {}

  ionViewDidLoad() {
    console.log('ionViewDidLoad BenefitFormPage');
    this.paymentType = this.navParams.get('paymentType');
    this.taxId = this.navParams.get('tin');

    let loader = this.controller.showLoader('Loading...');
    loader.present();

    this.getExitReason(this.paymentType);
    this.appDocData = [];
    this.getPenPin();
    this.requiredDocs();

    loader.dismiss();
  }

  async getPenPin() {
    let loginResponse = await this.store.fetchDoc('logindata');
    let userResponse = await this.store.fetchDoc('user');

    this.pencomPin = loginResponse.pencomPin;
    if (userResponse != 'Failed' && userResponse.avcValue != '') {
      let avcString: string = userResponse.avcValue;
      this.avcValue = Number(avcString.replace(/,/g, ''));
    }
  }

  async requiredDocs() {
    let loader = this.controller.showLoader('Loading...');
    loader.present();
    this.documentData = [];
    let data = {
      AppType: Number.parseInt(this.paymentType),
      Sector: 0
    };
    var funcName = '/BenfitRequiredDocument';

    try {
      let response = await this.serverService.processData(data, funcName);

      if (response.StatusCode == 'FF') {
        this.controller.showToast(response.Message, 'bottom');
        this.leavePage();
      }
      else if (response.StatusCode == '00') {
        this.image = this.appServer.leadwayImage();
        for (let val of response.Data) {
          if (val.documentNameField != 'Application Form' && val.documentNameField != 'RSA Snapshot') {
            this.documentData.push({
              docId: val.documentIDField,
              docType: val.documentNameField,
              appId: val.applicationTypeIDField,
              image: 'assets/images/add_image.png',
              photo: 'assets/images/add_image.png'
            });
          }
          else {
            this._documentData.push({
              docId: val.documentIDField,
              docType: val.documentNameField,
              appId: val.applicationTypeIDField,
              image: this.image,
              photo: this.image
            });
          }
        }
      }
      else {
        this.controller.showToast('Unable to get required documents. Check your network connection.', 'bottom');
        this.leavePage();
      }
    }
    catch(err) {
      console.log(err);
      this.controller.showToast('Error occurred. Try again later.', 'bottom');
      this.leavePage();
    }
    loader.dismiss();
  }

  ionViewDidEnter() {
   this.platform.registerBackButtonAction(() => {
     this.leavePage();
   });
  }

  selectBank() {
    let modalCtrl = this.controller.showBankModal(BranchModalPage, '');
    modalCtrl.onDidDismiss(data => {
      if (data != 'cancel') {
        this.bank = data.bankNameField;
        this.bankId = data.bankIDField
      }
    });
    modalCtrl.present();
  }

  selectBankBranch() {
    if (this.bank == '') {
      this.controller.showToast('Select a bank', 'bottom');
      return false;
    }
    let data = {
      bankId: this.bankId
    };
    let modalCtrl = this.controller.showBankModal(BankbranchModalPage, data);

    modalCtrl.onDidDismiss(data => {
      console.log(data);
      if (data != 'cancel') {
        this.bankBranch = data.bankBranchNameField;
        this.bankBranchId = data.branchIDField;
      }
    });
    modalCtrl.present();
  }

  async getExitReason(val) {
    if (val == '2') {
      let data = {
        appTypeID: Number.parseInt(val)
      };
      var funcName = '/GetExitReasons';

      try {
        let response = await this.serverService.processData(data, funcName);
        if (response.StatusCode == '00') {
          this.disengageData = response.Data;
        }
      }
      catch(err) {
        console.log(err);
        this.controller.showToast('Error occurred. Try again later.', 'bottom');
        this.leavePage();
      }
    }
  }

  async uploadImage(docData, val) {
    let imageResponse = (val == 'camera') ? await this.native.uploadFromCamera() : await this.native.uploadFromLibrary();
    console.log(imageResponse);

    if (imageResponse != 'Failed') {
      docData.photo = imageResponse;
      docData.image = this.sanitize.bypassSecurityTrustUrl(imageResponse);
    }
  }

  leavePage() {
    this.navCtrl.pop();
  }

  async submit() {
    this.appDocs();
    const testString = /[^0-9]/g;

    if (this.designation.trim() == '' || this.department.trim() == '' || this.acctName.trim() == '' || this.acctNum.trim() == '' || this.bvn.trim() == '' || this.bank.trim() == '' || this.bankBranch.trim() == '') {
      this.controller.showToast('All fields are required', 'bottom');
      return false;
    }

    if (this.paymentType == '7') {
      if (this.amount.toString() == null || this.amount.toString().trim() == '') {
        this.controller.showToast('Enter amount', 'bottom');
        return false;
      }
      else if (this.amount > this.avcValue) {
        this.controller.showToast(`AVC Value: ${this.avcValue}. Withdrawal amount cannot be more than your AVC Value`, 'bottom');
        return false;
      }
    }
    else {
      if (this.disengageRsn.trim() == '') {
        this.controller.showToast('Select reason for disengagement', 'bottom');
        return false;
      }
    }

    if (this.acctNum.match(testString) || this.acctNum.length < 10) {
      this.controller.showToast('Invalid account number', 'bottom');
      return false;
    }

    if (this.bvn.match(testString) || this.bvn.length < 11) {
      this.controller.showToast('Invalid BVN', 'bottom');
      return false;
    }

    console.log(this.appDocData);
    if (this.appDocData.length < 1) {
      this.controller.showToast('Capture all documents above', 'bottom');
      return false;
    }

    for (let val of this.appDocData) {
      console.log(val.documentTypeName);
      console.log(val.documentHashValue);
      if (val.documentTypeName != 'Application Form' && val.documentTypeName != 'RSA Snapshot') {
        if (val.documentHashValue == 'assets/images/add_image.png') {
          this.controller.showToast('Capture all documents above', 'bottom');
          return false;
        }
      }
    }

    let _data = {
      email: '',
      pencomPin: this.pencomPin,
      mobileNo: '',
      OTPType: 3
    };
    var _funcName = "/ValidateUser";

    try {
      let validateResponse = await this.serverService.processData(_data, _funcName);

      if (validateResponse.StatusCode == '00') {
        this.showOtpAlert();
      }
      else if (validateResponse.StatusCode == 'FF') {
        this.controller.showAlert(validateResponse.Message);
      }
      else {
        this.controller.showToast('Network error. Check your network and try again.', 'bottom');
      }
    }
    catch(err) {
      console.log(err);
      this.controller.showToast('Error occurred. Try again later.', 'bottom')
    }
  }

  async submitBenefitApp(otp) {
    let loader = this.controller.showLoader("Please wait...");
    loader.present();

    let _data = {
      PencomPin: this.pencomPin,
      OTPType: 3,
      otp: otp
    };
    var validateOTPFunction = "/ValidateOTP";

    let data = {
      aVCApplicationAmount: this.amount,
      accountName: this.acctName,
      accountNumber: this.acctNum,
      customerBankID: Number.parseInt(this.bankId),
      customerBankBranchID: Number.parseInt(this.bankBranchId),
      applicationDocuments: this.appDocData,
      reason: this.disengageRsn,
      // reason: '11-11-1900',
      tIN: this.taxId,
      applicationTypeName: this.navParams.get('paymentName'),
      pIN: this.pencomPin,
      appTypeId: Number.parseInt(this.paymentType),
      createdBy: "",
      department: this.department,
      designation: this.designation,
      OTP: otp
    }
    var funcName = '/SubmitBenefitApplication';

    try {
      let response = await this.serverService.processData(_data, validateOTPFunction);

      if (response.StatusCode == 'FF') {
        this.controller.showToast(response.Message, 'bottom');
        this.showOtpAlert();
      }
      else if (response.StatusCode == '00') {
        let benefitResponse = await this.serverService.processData(data, funcName);

        if (benefitResponse.StatusCode == 'FF') {
          this.controller.showToast(benefitResponse.Message, 'bottom');
        }
        else if (benefitResponse.StatusCode == '00') {
          this.controller.showToast(benefitResponse.Message + ' ' + benefitResponse.Data, 'bottom');
          this.controller.showAlert('Application Number: ' + benefitResponse.Data);
          this.leavePage();
        }
        else {
          this.controller.showToast('Network error occurred. Check your connection.', 'bottom');
        }
      }
      else {
        this.controller.showToast('Network error occurred. Check your connection.', 'bottom');
      }
    }
    catch(err) {
      console.log(err);
      this.controller.showToast('Error occurred. Try again later.', 'bottom');
    }
    loader.dismiss();
  }

  showOtpAlert() {
    let alertOptions: AlertOptions = {
      message: 'Enter the OTP code that was sent to your registered mobile phone and email address',
      enableBackdropDismiss: false,
      inputs: [
        {
          name: 'otp',
          type: 'number',
          placeholder: 'Enter OTP code'
        }
      ],
      buttons: [
        {
          text: 'Cancel',
          role: 'cancel'
        },
        {
          text: 'Submit',
          handler: data => {
            console.log(data);
            if (data.otp == "") {
              this.controller.showToast("OTP code cannot be empty", "bottom");
              this.showOtpAlert();
            } else {
              this.submitBenefitApp(data.otp);
            }
          }
        }
      ]
    };
    let alert = this.alertCtrl.create(alertOptions);
    alert.present();
  }

  captureSignature(val) {
    let popOptions: PopoverOptions = {
      enableBackdropDismiss: false,
      showBackdrop: true
    };
    let popAction = this.popCtrl.create(SignaturePadPage, popOptions);
    popAction.onDidDismiss((data) => {
      if (data != 'cancel') {
        let sign: string = data;
        val.image = sign.replace('data:image/png', 'data:image/jpeg');
        val.photo = sign.replace('data:image/png', 'data:image/jpeg');
      }
    });
    popAction.present();
  }

  appDocs() {
    var _appDocData = [];
    var NewArray = [];
    Array.prototype.push.apply(NewArray, this._documentData);
    Array.prototype.push.apply(NewArray, this.documentData);

    var date = new Date();
    var month = (date.getMonth() < 9) ? '0' + (date.getMonth() + 1).toString() : (date.getMonth() + 1).toString();
    var day = (date.getDate() < 10) ? '0' + date.getDate().toString() : date.getDate().toString();
    var minutes = (date.getMinutes() < 10) ? '0' + date.getMinutes().toString() : date.getMinutes().toString();
    var seconds = (date.getSeconds() < 10) ? '0' + date.getSeconds().toString() : date.getSeconds().toString();

    let dateReceived = date.getFullYear().toString() + '-' + month + '-' + day;
    let timeReceived = date.getHours().toString() + ':' + minutes + ':' + seconds;

    for (let val of NewArray) {
      _appDocData.push({
        documentHashValue: val.photo,
        isVerified: "",
        documentLocation: "",
        documentTypeName: val.docType,
        documentTypeID: val.docId,
        dateReceived: dateReceived + 'T' + timeReceived,
        receivedBy: "",
        memberApplicationID: "",
        documentSource: ""
      });
    }
    this.appDocData = _appDocData;
    console.log(this.appDocData);
  }

  documentUploadPicker(docData) {
    if (docData.docType == 'Application letter') {
      this.signatureCapture(docData);
    }
    else {
      let actionSheetOptions: ActionSheetOptions = {
        title: 'Upload from',
        enableBackdropDismiss: true,
        buttons: [
          {
            text: 'Camera',
            handler: () => {
              this.uploadImage(docData, 'camera');
            }
          },
          {
            text: 'Photo Library',
            handler: () => {
              this.uploadImage(docData, 'library');
            }
          }
        ]
      }
      this.actionSheet.create(actionSheetOptions).present();
    }
  }

  signatureCapture(docData) {
    let actionSheetOptions: ActionSheetOptions = {
      title: 'Capture signature from',
      enableBackdropDismiss: true,
      buttons: [
        {
          text: 'Signature Pad',
          handler: () => {
            this.captureSignature(docData);
          }
        },
        {
          text: 'Camera',
          handler: () => {
            this.uploadImage(docData, 'camera');
          }
        }
      ]
    }
    this.actionSheet.create(actionSheetOptions).present();
  }

}
