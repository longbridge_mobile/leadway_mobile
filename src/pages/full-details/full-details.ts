import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import { StorageService } from '../../providers/storage-service';

@Component({
  selector: 'page-full-details',
  templateUrl: 'full-details.html',
})
export class FullDetailsPage {
  title: string = '';
  firstName: string = '';
  middleName: string = '';
  lastName: string = '';
  email: string = '';
  address: string = '';
  mobileNo: string = '';
  dob: string = '';
  gender: string = '';
  stateOfOrigin: string = '';
  lga: string = '';

  nokTitle: string = '';
  nokName: string = '';
  nokEmail: string = '';
  nokMobileNo: string = '';
  nokDob: string = '';

  employerName: string = '';
  dateOfEmployment: string = '';
  employerAddress: string = '';
  lastReceivedFundDate: string = '';

  erroMessage: string = '';

  constructor(
    private navCtrl: NavController,
    private store: StorageService
  ) {}

  ionViewDidLoad() {
    console.log('ionViewDidLoad FullDetailsPage');
    this.getPersonalDetails();
  }

  async getPersonalDetails() {
    let userResponse = await this.store.fetchDoc('userinfo');
    let employerResponse = await this.store.fetchDoc('employer');
    let kinResponse = await this.store.fetchDoc('nextofkin');

    if (userResponse != "Failed" && employerResponse != "Failed" && kinResponse != "Failed") {
      this.title = userResponse.title;
      this.firstName = userResponse.firstname;
      this.lastName = userResponse.lastName;
      this.middleName = userResponse.middleName;
      this.email = userResponse.email;
      this.address = userResponse.address;
      this.mobileNo = userResponse.mobileNo;
      this.dob = userResponse.dob;
      this.gender = userResponse.gender;
      this.stateOfOrigin = userResponse.stateOfOrigin;
      this.lga = userResponse.lga;

      this.nokName = kinResponse.firstName + ' ' + kinResponse.lastName;
      this.nokTitle = kinResponse.title;
      this.nokMobileNo = kinResponse.mobileNo;
      this.nokDob = kinResponse.dob;
      this.nokEmail = kinResponse.email;

      this.employerAddress = employerResponse.address;
      this.employerName = employerResponse.name;
      this.dateOfEmployment = employerResponse.date;
    }
    else {
      this.erroMessage = 'Unable to get details. ';
    }
  }

}
