import { Component } from '@angular/core';
import { NavController, Platform, App } from 'ionic-angular';
import { EditProfilePage } from '../edit-profile/edit-profile';
import { EditEmployerPage } from '../edit-employer/edit-employer';
import { ChangePinPage } from '../change-pin/change-pin';
import { SubscriptionPage } from '../subscription/subscription';
import { ControllerService } from '../../providers/controller-service';
import { LoginPage } from '../login/login';
import { StorageService } from '../../providers/storage-service';

@Component({
  selector: 'page-settings',
  templateUrl: 'settings.html',
})
export class SettingsPage {

  constructor(
    public navCtrl: NavController, 
    private controller: ControllerService,
    private platform: Platform,
    private app: App,
    private store: StorageService
  ) {}

  ionViewDidLoad() {
    console.log('ionViewDidLoad SettingsPage');
  }

  ionViewDidEnter(){
   this.platform.registerBackButtonAction(() => {
     this.leavePage();
   });
  }

  leavePage() {
    this.navCtrl.pop();
  }

  changePersonalData() {
    this.navCtrl.push(EditProfilePage);
  }

  changeEmployerData() {
    this.navCtrl.push(EditEmployerPage);
  }

  changePin() {
    this.navCtrl.push(ChangePinPage);
  }

  subscription() {
    this.navCtrl.push(SubscriptionPage);
  }

  async signOut() {
    this.app.getRootNav().setRoot(LoginPage, {
      timedout: false
    });
    let response = await this.store.fetchDoc('logindata');

    if (response != 'Failed') {
      response.sessionId = '';
      
      await this.store.createUpdateDoc(response);
    }
  }
}
