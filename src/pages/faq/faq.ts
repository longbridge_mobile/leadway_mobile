import { Component } from '@angular/core';
import { NavController, Platform } from 'ionic-angular';
import { ServerServiceProvider } from '../../providers/server-service/server-service';
import { ControllerService } from '../../providers/controller-service';
import { FaqModalPage } from '../faq-modal/faq-modal';

@Component({
  selector: 'page-faq',
  templateUrl: 'faq.html',
})
export class FaqPage {
  faqData: any = [];

  constructor(
    private serverService: ServerServiceProvider,
    private controller: ControllerService,
    private navCtrl: NavController,
    private platform: Platform
  ) {}

  ionViewDidLoad() {
    console.log('ionViewDidLoad FaqPage');
    this.getFAQ();
  }

  ionViewDidEnter(){
    this.platform.registerBackButtonAction(() => {
      this.leavePage();
    });
  }

  async getFAQ() {
    let loader = this.controller.showLoader("Loading...");
    loader.present();

    try {
      let serverResponse = await this.serverService.processData('', '/FAQQuestionsAndAnswer');
      console.log(serverResponse);
      
      if (serverResponse.StatusCode == "00") {
        this.faqData = serverResponse.Data;

        if (this.faqData.length < 1) {
          this.controller.showToast("FAQ unavailable", "bottom");
          this.navCtrl.pop();
        }
      }
      else if (serverResponse.StatusCode == "FF") {
        this.controller.showToast(serverResponse.Message, 'bottom');
      }
    }
    catch(err) {
      console.log(err);
      this.controller.showToast("Network error. Try again later.", "bottom");
    }
    loader.dismiss();
  }

  openFAQModal(selectedFAQ) {
    let data = {
      name: selectedFAQ.Questions,
      details: selectedFAQ.Answer,
      title: 'FAQ'
    };
    this.controller.showModal(FaqModalPage, data);
  }

  leavePage() {
    this.navCtrl.pop();
  }

  doRefresh(val) {
    this.faqData = [];
    setTimeout(() => {
      val.complete();
    }, 1000);
    this.getFAQ();
  }

}
