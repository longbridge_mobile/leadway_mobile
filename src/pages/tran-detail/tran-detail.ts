import { Component } from '@angular/core';
import { NavParams, Platform, NavController } from 'ionic-angular';
import { ControllerService } from '../../providers/controller-service';

@Component({
  selector: 'page-tran-detail',
  templateUrl: 'tran-detail.html',
})
export class TranDetailPage {
  tranData: any = [];
  contributionDate: string = '';
  unitPrice: string = '';
  unit: string = '';
  erContribution: string = '';
  eeContribution: string = '';
  netContribution: string = '';

  constructor(
    private navCtrl: NavController, 
    public navParams: NavParams,
    private controller: ControllerService,
    private platform: Platform
  ) {}

  ionViewDidLoad() {
    console.log('ionViewDidLoad TranDetailPage');
    this.tranData = this.navParams.get('tranData');
    this.contributionDate = this.tranData.dateField;
    this.unitPrice = '₦' + this.tranData.unitpriceField;
    this.unit = this.tranData.unitsField;
    this.erContribution = '₦' + this.tranData.erconsField;
    this.eeContribution = '₦' + this.tranData.eeconsField;
    this.netContribution = '₦' + this.tranData.netcontributionField;
  }

  ionViewDidEnter() {
    this.platform.registerBackButtonAction(() => {
     this.leavePage();
   });
  }

  leavePage() {
    this.navCtrl.pop();
  }

}
