import { Component } from '@angular/core';
import { NavController, NavParams, Platform } from 'ionic-angular';

@Component({
  selector: 'page-view-message',
  templateUrl: 'view-message.html',
})
export class ViewMessagePage {
  message: string = '';
  title: string = '';

  constructor(public navCtrl: NavController, public navParams: NavParams, private platform: Platform) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad ViewMessagePage');
    this.message = this.navParams.get('message');
    this.title = this.navParams.get('title');
  }

  ionViewDidEnter() {
   this.platform.registerBackButtonAction(() => {
     this.leavePage();
   });
  }

  leavePage() {
    this.navCtrl.pop();
  }

}
