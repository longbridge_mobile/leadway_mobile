import { Component } from '@angular/core';
import { NavController, NavParams, Platform } from 'ionic-angular';
import { AccountPage } from '../account/account';
import { BenefitsPage } from '../benefits/benefits';
import { ProfilePage } from '../profile/profile';
import { HomePage } from '../home/home';
import { NotificationPage } from '../notification/notification';

@Component({
  selector: 'page-tab',
  templateUrl: 'tab.html',
})
export class TabPage {
  tab1: any = AccountPage;
  tab2: any = BenefitsPage;
  tab3: any = NotificationPage;
  tab4: any = ProfilePage;
  tab5: any = HomePage;

  constructor(public navCtrl: NavController, public navParams: NavParams, public platform: Platform) {}

  ionViewDidLoad() {
    console.log('ionViewDidLoad TabPage');
  }

}
