import { Component } from '@angular/core';
import { NavController, Platform } from 'ionic-angular';
import { NativeService } from '../../providers/native-service';
import { Device } from '@ionic-native/device';

@Component({
  selector: 'page-about',
  templateUrl: 'about.html',
})
export class AboutPage {
  versionNumber: string = '';
  deviceOs: string = '';
  deviceManufacturer: string = '';
  deviceVersion: string = '';

  constructor(public navCtrl: NavController, public platform: Platform, private native: NativeService, private device: Device) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad AboutPage');
    this.deviceInfo();
    this.getAppVersion();
  }

  ionViewDidEnter(){
    this.platform.registerBackButtonAction(() => {
      this.leavePage();
    });
  }

  leavePage() {
    this.navCtrl.pop();
  }

  async getAppVersion() {
    this.versionNumber = await this.native.getAppVersion();
  }

  deviceInfo() {
    try {
      this.deviceOs = this.device.platform;
      this.deviceManufacturer = this.device.manufacturer;
      this.deviceVersion = this.device.version;
    } catch (error) {
      console.log(error);
      this.deviceOs = 'N/A';
      this.deviceManufacturer = 'N/A';
      this.deviceVersion = 'N/A';
    }
  }

}
