import { Component } from '@angular/core';
import { NavController, Platform, App } from 'ionic-angular';
import { ControllerService } from '../../providers/controller-service';
import { StorageService } from '../../providers/storage-service';
import { ServerServiceProvider } from '../../providers/server-service/server-service';
import { NewPinPage } from '../new-pin/new-pin';

@Component({
  selector: 'page-change-pin',
  templateUrl: 'change-pin.html',
})
export class ChangePinPage {
  oldPin: string = '';
  newPin: string = '';
  _newPin: string = '';

  constructor(
    public navCtrl: NavController,
    private controller: ControllerService,
    private store: StorageService,
    private server: ServerServiceProvider,
    private platform: Platform,
    private app: App
  ) {}

  ionViewDidLoad() {
    console.log('ionViewDidLoad ChangePinPage');
  }

  ionViewDidEnter(){
    this.platform.registerBackButtonAction(() => {
      this.leavePage();
    });
  }

  submit() {
    if (this.oldPin == '') {
      this.controller.showToast('Please enter your mobile pin', 'bottom');
      return false;
    }

    this.app.getRootNav().push(NewPinPage, {
      otp: 'change',
      oldPin: this.oldPin
    });
  }

  leavePage() {
    this.navCtrl.pop();
  }

}
