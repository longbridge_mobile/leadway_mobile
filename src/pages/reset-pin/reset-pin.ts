import { Component } from '@angular/core';
import { NavController, Platform } from 'ionic-angular';
import { ServerServiceProvider } from '../../providers/server-service/server-service';
import { StorageService } from '../../providers/storage-service';
import { AlertController, AlertOptions } from 'ionic-angular';
import { ControllerService } from '../../providers/controller-service';
import { NewPinPage } from '../new-pin/new-pin';

@Component({
  selector: 'page-reset-pin',
  templateUrl: 'reset-pin.html',
})
export class ResetPinPage {
  userType: string = 'pin';
  disableButton: boolean = true;
  email: string = '';
  pencomPin: string = 'PEN';
  _penPin: string = '';
  mobileNo: string = '';

  alertOptions: AlertOptions;

  constructor(
    private controller: ControllerService,
    private alertCtrl: AlertController,
    private navCtrl: NavController,
    private serverSevice: ServerServiceProvider,
    private platform: Platform,
    private store: StorageService
  ) {}

  ionViewDidLoad() {
    console.log('ionViewDidLoad ResetPinPage');
  }

  ionViewDidEnter(){
   this.platform.registerBackButtonAction(() => {
      this.leavePage();
    });
  }

  async submit() {
    if (this.email != '') {
      if (!this.validateEmail(this.email)) {
        this.controller.showToast('Invalid email entered', 'bottom');
        return false;
      }
    }

    if (this.mobileNo != '') {
      if (!this.validatePhoneNumber(this.mobileNo)) {
        this.controller.showToast('Invalid phone number', 'bottom');
        return false;
      }
    }

    let data = {
      email: this.email,
      pencomPin: this.pencomPin,
      mobileNo: this.mobileNo,
      OTPType: 2
    };
    var funcName = "/ValidateUser";

    let loader = this.controller.showLoader("Validating user...");
    loader.present();

    try {
      let serverResponse = await this.serverSevice.processData(data, funcName);
      console.log(serverResponse);

      if (serverResponse.StatusCode == "FF") {
        this.controller.showAlert(serverResponse.Message);
      }
      else if (serverResponse.StatusCode == "00") {
        this._penPin = serverResponse.Data;
        this.showOtpAlert();
      }
      else {
        this.controller.showToast('Network error. Check your network connection.', 'bottom');
      }
    }
    catch(err) {
      console.log(err);
      this.controller.showToast("Network error. Try again", "bottom");
    }
    loader.dismiss();
  }

  async changePin(otpData) {
    console.log(otpData.otp);
    let loader = this.controller.showLoader("Please wait...");
    loader.present();

    let data = {
      PencomPin: this._penPin,
      OTPType: 2,
      otp: otpData.otp
    };
    var validateOTPFunction = "/ValidateOTP";

    try {
      let otpResponse = await this.serverSevice.processData(data, validateOTPFunction);
      console.log(otpResponse);

      if (otpResponse.StatusCode == "FF") {
        this.controller.showToast(otpResponse.Message, 'bottom');
        this.showOtpAlert();
      }
      else if (otpResponse.StatusCode == "00") {
        this.controller.showToast(otpResponse.Message, 'bottom');
        
        let loginResponse = await this.store.fetchDoc('logindata');
        console.log(loginResponse);
        loginResponse.pencomPin = this._penPin;

        let response = await this.store.createUpdateDoc(loginResponse);
        console.log(response);

        this.navCtrl.push(NewPinPage, {
          otp: otpData.otp,
          OTPType: 1
        });
      }
    }
    catch(err) {
      console.log(err);
      this.controller.showToast("Network error. Try again.", "bottom");
    }
    loader.dismiss();
  }

  swipeEvent(val) {
    console.log(val.direction);

    if (val.direction == 2) {
      if (this.userType == "pin") {
        this.userType = 'email';
      } 
      else if (this.userType == "email") {
        this.userType = 'mobile';
      }
    }
    else if (val.direction == 4) {
      if (this.userType == 'mobile') {
        this.userType = 'email';
      }
      else if (this.userType == 'email') {
        this.userType = 'pin';
      }
    }
  }

  enableSubmitButton(val) {
    if (val == 'pin') {
      this.pencomPin = this.pencomPin.trim();
      this.email = '';
      this.mobileNo = '';

      if (this.pencomPin != "") {
        this.disableButton = false;
      }
      else {
        this.disableButton = true;
      }
    }
    else if (val == 'email') {
      this.email = this.email.trim();
      this.pencomPin = '';
      this.mobileNo = '';

      if (this.email != "") {
        this.disableButton = false;
      }
      else {
        this.disableButton = true;
      }
    }
    else if (val == 'mobile') {
      this.mobileNo = this.mobileNo.trim();
      this.pencomPin = '';
      this.email = '';

      if (this.mobileNo != "") {
        this.disableButton = false;
      }
      else {
        this.disableButton = true;
      }
    }
  }

  validateEmail(email): boolean {
    var atpos = email.indexOf('@');
		var dotpos = email.lastIndexOf('.');

		if (atpos<1 || dotpos<atpos+2 || dotpos+2>=email.length) {
		  return false;
		}
    else {
      return true;
    }
  }

  validatePhoneNumber(phoneNum: string): boolean {
    if (isNaN(Number(phoneNum))) {
      console.log('Match');
      return false;
    }
    else {
      return true;
    }
  }

  showOtpAlert() {
    this.alertOptions = {
      message: 'Enter the OTP code that was sent to your registered mobile phone and email address',
      enableBackdropDismiss: false,
      inputs: [
        {
          name: 'otp',
          type: 'number',
          placeholder: 'Enter OTP code'
        }
      ],
      buttons: [
        {
          text: 'Cancel',
          role: 'cancel'
        },
        {
          text: 'Submit',
          handler: data => {
            console.log(data);
            if (data.otp == "") {
              this.controller.showToast("OTP code cannot be empty", "bottom");
              this.showOtpAlert();
            } else {
              this.changePin(data);
            }
          }
        }
      ]
    };
    let alert = this.alertCtrl.create(this.alertOptions);
    alert.present();
  }

  leavePage() {
    this.navCtrl.pop();
  }

}
