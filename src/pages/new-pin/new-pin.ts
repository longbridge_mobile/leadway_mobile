import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import { IonDigitKeyboard, IonDigitKeyboardOptions } from '../../components/ion-digit-keyboard/ion-digit-keyboard';
import { ConfirmNewPinPage } from '../confirm-new-pin/confirm-new-pin';
import { ControllerService } from '../../providers/controller-service';

@Component({
  selector: 'page-new-pin',
  templateUrl: 'new-pin.html',
})
export class NewPinPage {
  keyboardSettings: IonDigitKeyboardOptions = {
        align: 'center',
        width: '95%',
        visible: true,
        leftActionOptions: {
            iconName: 'backspace',
            fontSize: '1.2em'
        },
        rightActionOptions: {
            iconName: 'checkmark-circle',
            // text: 'Enter',
            fontSize: '1.2em'
        },
        roundButtons: false,
        showLetters: false,
        swipeToHide: true,
        theme: 'opaque-white'
    };

    input1: string = "";
    input2: string = "";
    input3: string = "";
    input4: string = "";
    mobilePin: string = "";

  constructor(public navCtrl: NavController, public controller: ControllerService, private navParam: NavParams) {}

  ionViewDidLoad() {
    console.log('ionViewDidLoad NewPinPage');
  }

  onKeyboardButtonClick(key: any) {
    console.log(key);
    if (key != "right" && key != "left") {
      if (this.input1 == "") {
        this.input1 = key.toString();
      } 
      else if (this.input2 == "") {
        this.input2 = key.toString();
      }
      else if (this.input3 == "") {
        this.input3 = key.toString();
      }
      else if (this.input4 == "") {
        this.input4 = key.toString();
      }
    }

    if (key == "left") {
      if (this.input4 != "") {
        this.input4 = "";
      } else if (this.input3 != "") {
        this.input3 = "";
      } else if (this.input2 != "") {
        this.input2 = "";
      } else if (this.input1 != "") {
        this.input1 = "";
      }
    }

    if (key == "right") {
      this.mobilePin = this.input1 + this.input2 + this.input3 + this.input4;
      console.log(this.mobilePin);

      if (this.mobilePin.length < 4) {
        this.controller.showToast("Mobile pin cannot be less than 4 digits", "bottom");
      } else {
        if (this.navParam.get("otp") == 'change'){
          this.navCtrl.push(ConfirmNewPinPage, {
            oldPin: this.navParam.get("oldPin"),
            newPin: this.mobilePin,
            otp: this.navParam.get("otp")
          });
        }
        else {
          this.navCtrl.push(ConfirmNewPinPage, {
            newPin: this.mobilePin,
            otp: this.navParam.get("otp"),
            pencomPin: this.navParam.get('pencomPin'),
            OTPType: this.navParam.get('OTPType')
          });
        }
      }
    }

  }

  close() {
    this.navCtrl.pop();
  }

}
