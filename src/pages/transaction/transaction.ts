import { Component } from '@angular/core';
import { NavController, Platform, NavParams } from 'ionic-angular';
import { TranDetailPage } from '../tran-detail/tran-detail';
import { ControllerService } from '../../providers/controller-service';

@Component({
  selector: 'page-transaction',
  templateUrl: 'transaction.html',
})
export class TransactionPage {
  transactionData: any = "";

  constructor(
    public navCtrl: NavController,
    private controller: ControllerService,
    private platform: Platform,
    private navParam: NavParams
  ) {}

  ionViewDidLoad() {
    console.log('ionViewDidLoad TransactionPage');
    this.transactionData = this.navParam.get('transactionData');
  }

  ionViewDidEnter(){
   this.platform.registerBackButtonAction(() => {
     this.closePage();
   });
  }

  closePage() {
    this.navCtrl.pop();
  }

  tranDetails(val) {
    this.navCtrl.push(TranDetailPage, {
      tranData: val
    });
  }

}
