import { Component } from '@angular/core';
import { ViewController, NavParams } from 'ionic-angular';
import { ServerServiceProvider } from '../../providers/server-service/server-service';
import { ControllerService } from '../../providers/controller-service';

@Component({
  selector: 'page-branch-modal',
  templateUrl: 'branch-modal.html',
})
export class BranchModalPage {
  bankData: any = [];
  _bankData: any = [];

  constructor(
    public viewCtrl: ViewController, 
    public navParams: NavParams,
    private serverService: ServerServiceProvider,
    private controller: ControllerService
  ) {}

  ionViewDidLoad() {
    console.log('ionViewDidLoad BranchModalPage');
    this.getBanks();
  }

  async getBanks() {
    let loader = this.controller.showLoader('Getting list...');
    loader.present();

    try {
      let response = await this.serverService.processData('', '/GetBanks');

      if (response.StatusCode == '00') {
        var newBankData = [];
        newBankData = response.Data;
        for (let val of newBankData) {
          if (val.bankNameField != '') {
            this._bankData.push(val);
            this.bankData.push(val);
          }
        }
      } else {
        this.controller.showToast('Error occurred. Try again later.', 'bottom');
        this.selectedBank('Error');
      }
    }
    catch(err) {
      console.log(err);
      this.controller.showToast('Error occurred. Try again later.', 'bottom');
      this.selectedBank('Error');
    }

    loader.dismiss();
  }

  selectedBank(data) {
    this.viewCtrl.dismiss(data);
  }

  getItems(ev) {
    this.bankData = this._bankData 
    var val = ev.target.value;

    if (val && val.trim() != '') {
      this.bankData = this.bankData.filter((item) => {
        return (item.bankNameField.toLowerCase().indexOf(val.toLowerCase()) > -1);
      });
    }
  }

}
