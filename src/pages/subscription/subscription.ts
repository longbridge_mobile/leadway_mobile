import { Component } from '@angular/core';
import { NavController, Platform } from 'ionic-angular';
import { StorageService } from '../../providers/storage-service';
import { ServerServiceProvider } from '../../providers/server-service/server-service';
import { ControllerService } from '../../providers/controller-service';

@Component({
  selector: 'page-subscription',
  templateUrl: 'subscription.html',
})
export class SubscriptionPage {
  birthdayEmail: boolean;
  newsletterEmail: boolean;
  printedStatement: boolean;
  mailStatement: boolean;
  birthdaySMS: boolean;
  smsContribution: boolean;
  showList: boolean  = false;

  constructor(
    public navCtrl: NavController,
    private store: StorageService,
    private serverService: ServerServiceProvider,
    private controller: ControllerService,
    private platform: Platform
  ) {}

  ionViewDidLoad() {
    console.log('ionViewDidLoad SubscriptionPage');
    this.fetchServerSettings();
  }

  ionViewDidEnter(){
    this.platform.registerBackButtonAction(() => {
      this.leavePage();
    });
  }

  async fetchExistingSettings() {
    let subResponse = await this.store.fetchDoc('subscription');
    console.log(subResponse);

    if (subResponse != "Failed") {
      this.birthdayEmail = subResponse.birthdayEmail;
      this.newsletterEmail = subResponse.newsletterEmail;
      this.printedStatement = subResponse.printedStatement;
      this.mailStatement = subResponse.mailStatement;
      this.birthdaySMS = subResponse.birthdaySMS;
      this.smsContribution = subResponse.smsContribution;
      this.showList = true;
    }
    else {
      await this.fetchServerSettings();
    }
  }

  async fetchServerSettings() {
    this.showList = false;
    let loader = this.controller.showLoader('Loading settings...');
    loader.present();

    let loginResponse = await this.store.fetchDoc('logindata');
    let body = {
      sessionID: loginResponse.sessionId
    };
    var funcName = '/PreferenceSetup';

    try {
      let serverResponse = await this.serverService.processData(body, funcName);
      console.log(serverResponse);

      if (serverResponse.StatusCode == "00") {
        this.birthdayEmail = serverResponse.Data.email_birthdayField;
        this.newsletterEmail = serverResponse.Data.email_newsletterField;
        this.printedStatement = serverResponse.Data.print_statementsField;
        this.mailStatement = serverResponse.Data.email_statementsField;
        this.birthdaySMS = serverResponse.Data.sms_birthdayField;
        this.smsContribution = serverResponse.Data.sms_contribution_notificationField;
        this.showList = true;

        let response = await this.store.fetchDoc('subscription');
        let doc = {
          _id: 'subscription',
          birthdayEmail: serverResponse.Data.email_birthdayField,
          newsletterEmail: serverResponse.Data.email_newsletterField,
          printedStatement: serverResponse.Data.print_statementsField,
          mailStatement: serverResponse.Data.email_statementsField,
          birthdaySMS: serverResponse.Data.sms_birthdayField,
          smsContribution: serverResponse.Data.sms_contribution_notificationField,
          _rev: response._rev
        };
        let docResponse = await this.store.createUpdateDoc(doc);
        console.log(docResponse);
      }
      else if (serverResponse.StatusCode == "FF"){
        this.controller.showToast(serverResponse.Message, 'bottom');
      }
    }
    catch(err) {
      console.log(err);
      this.controller.showToast('Unable to fetch updated subscription details', 'bottom');
    }
    loader.dismiss();
  }

  async updateSettings() {
    let loader = this.controller.showLoader('Updating settings...');
    loader.present();

    let loginResponse = await this.store.fetchDoc('logindata');
    let deviceResponse = await this.store.fetchDoc("device");

    let body = {
      bDayEmail: this.birthdayEmail,
      newsLetter: this.newsletterEmail,
      stateAccountPrinted: this.printedStatement,
      stateAccountEmail: this.mailStatement,
      bDaySMS: this.birthdaySMS,
      contibNotification: this.smsContribution,
      pencomPin: loginResponse.pencomPin,
      mobileId: deviceResponse.uuid
    };
    var funcName = '/updatePrefrence';

    try {
      let serverResponse = await this.serverService.processData(body, funcName);
      console.log(serverResponse);

      if (serverResponse.StatusCode == "00") {
        this.controller.showToast(serverResponse.Message, 'bottom');
        
        let response = await this.store.fetchDoc('subscription');
        let doc = {
          _id: 'subscription',
          birthdayEmail: this.birthdayEmail,
          newsletterEmail: this.newsletterEmail,
          printedStatement: this.printedStatement,
          mailStatement: this.mailStatement,
          birthdaySMS: this.birthdaySMS,
          smsContribution: this.smsContribution,
          _rev: response._rev
        };
        let docResponse = await this.store.createUpdateDoc(doc);
        console.log(docResponse);
      }
      else if (serverResponse.StatusCode == "FF") {
        this.controller.showToast(serverResponse.Message, 'bottom');
      }
      else {
        this.controller.showToast('Unable to update subscription. Try again later.', 'bottom');
      }
    }
    catch(err) {
      console.log(err);
      this.controller.showToast('Unable to update subscription. Try again later.', 'bottom');
    }
    loader.dismiss();
  }

  leavePage() {
    this.navCtrl.pop();
  }

  doRefresh(val) {
    setTimeout(() => {
      val.complete();
    }, 1000);
    this.fetchServerSettings();
  }

}
