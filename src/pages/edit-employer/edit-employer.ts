import { Component } from '@angular/core';
import { NavController, Platform } from 'ionic-angular';
import { StorageService } from '../../providers/storage-service';
import { ControllerService } from '../../providers/controller-service';
import { ServerServiceProvider } from '../../providers/server-service/server-service';
import { DatePicker, DatePickerOptions } from '@ionic-native/date-picker';

@Component({
  selector: 'page-edit-employer',
  templateUrl: 'edit-employer.html',
})
export class EditEmployerPage {
  employerName: string = '';
  startDate: string = '';
  _startDate: string = '';
  employerAddress: string = '';
  monthName = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'];

  constructor(
    public navCtrl: NavController,
    private controller: ControllerService,
    private store: StorageService,
    private serverService: ServerServiceProvider,
    private datePicker: DatePicker,
    private platform: Platform
  ) {}

  ionViewDidLoad() {
    console.log('ionViewDidLoad EditEmployerPage');
    this.fetchExisitingEmployerInfo();
  }

  ionViewDidEnter(){
    this.platform.registerBackButtonAction(() => {
      this.leavePage();
    });
  }

  async fetchExisitingEmployerInfo() {
    let employerResponse = await this.store.fetchDoc('employer');
    console.log(employerResponse);

    if (employerResponse != "Failed") {
      this.employerName = employerResponse.name;
      this.employerAddress = employerResponse.address;
      this.startDate = employerResponse.date;
      this._startDate = new Date(employerResponse.date).toDateString();
    }
    else {
      this.fetchEmployerFromServer();
    }
  }

  async fetchEmployerFromServer() {
    let loader = this.controller.showLoader('Getting details...');
    loader.present();

    let loginResponse = await this.store.fetchDoc('logindata');
    
    let body = {
      sessionID: loginResponse.sessionId
    };
    var funcName = '/FullClientDetails';

    try {
      let serverResponse = await this.serverService.processData(body, funcName);
      console.log(serverResponse);

      if (serverResponse.StatusCode == "00") {
        this.employerAddress = serverResponse.Data.officeaddress1;
        this.employerName = serverResponse.Data.employername;
        this.startDate = serverResponse.Data.date_of_first_employment;
        this._startDate = new Date(serverResponse.Data.date_of_first_employment).toDateString();
      }
      else if (serverResponse.StatusCode == "FF") {
        this.controller.showToast(serverResponse.Message, 'bottom');
      }
      else {
        this.controller.showToast('Unable to fetch existing details', 'bottom');
      }
    }
    catch(err) {
      console.log(err);
      this.controller.showToast('Unable to fetch existing details', 'bottom');
    }
    loader.dismiss();
  }

  async updateEmployerDetails() {
    if (this.employerAddress.trim() == '' || this.employerName.trim() == '' || this.startDate.trim() == '') {
      this.controller.showToast('All fields are required', 'bottom');
      return false;
    }
    
    let loader = this.controller.showLoader('Submitting update...');
    loader.present();

    let loginResponse = await this.store.fetchDoc('logindata');
    let deviceResponse = await this.store.fetchDoc('device');

    let body = {
      EmployerName: this.employerName,
      EmployerAddress: this.employerAddress,
      EmploymentDate: this.startDate,
      BasicSal: 0.0,
      AnnualSal: 0.0,
      TransaportSal: 0.0,
      HousingSal: 0.0,
      Pin: loginResponse.pencomPin,
      mobileId: deviceResponse.uuid
    };
    var funcName = '/ChangeEmployer';

    try {
      let serverResponse = await this.serverService.processData(body, funcName);
      console.log(serverResponse);

      if (serverResponse.StatusCode == '00') {
        this.controller.showToast(serverResponse.Message, 'bottom');
        this.leavePage();
      } 
      else if (!serverResponse.sucess) {
        this.controller.showToast(serverResponse.Message, 'bottom');
      }
      else {
        this.controller.showToast('Unable to update details. Try again later.', 'bottom');
      }
    }
    catch(err) {
      console.log(err);
      this.controller.showToast('Unable to update details. Try again later.', 'bottom');
    }
    loader.dismiss();
  }

  selectDate() {
    var pickedDate;
    // if (this.startDate == '') {
      pickedDate = new Date();
    // }
    // else {
    //   pickedDate = this._startDate;
    // }

    let dateOptions: DatePickerOptions = {
      mode: "date",
      date: pickedDate,
      androidTheme: this.datePicker.ANDROID_THEMES.THEME_HOLO_LIGHT
    };

    this.datePicker.show(dateOptions).then((val) => {
      var dayDate = (val.getDate() < 10) ? '0'+val.getDate() : val.getDate().toString();
      var monthDate = this.monthName[val.getMonth()];
      var fullYear = val.getFullYear().toString();

      this.startDate = dayDate + '-' + monthDate + '-' + fullYear;
      this._startDate = val.toDateString();
    })
    .catch((err) => {
      return '';
    });
  }

  doRefresh(val) {
    setTimeout(() => {
      val.complete();
    }, 1000);
    this.fetchEmployerFromServer();
  }

  leavePage() {
    this.navCtrl.pop();
  }

}
