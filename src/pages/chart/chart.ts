import { Component } from '@angular/core';
import { NavParams, NavController, Platform } from 'ionic-angular';
import { ControllerService } from '../../providers/controller-service';
import { ChartModalPage } from '../chart-modal/chart-modal';
import { ServerServiceProvider } from '../../providers/server-service/server-service';

@Component({
  selector: 'page-chart',
  templateUrl: 'chart.html',
})
export class ChartPage {
  fundType: string = '';
  duration: string = '';
  chartType: string = '';

  rsaFund: boolean;
  retireeFund: boolean;
  guinessGratuityFund: boolean;

  constructor(
    public navParams: NavParams,
    private controller: ControllerService,
    private serverService: ServerServiceProvider,
    private navCtrl: NavController,
    private platform: Platform
  ) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad ChartPage');
  }

  ionViewDidEnter(){
    this.platform.registerBackButtonAction(() => {
      this.leavePage();
    });
  }

  async displayChart() {
    if (this.fundType == '' || this.duration == '' || this.chartType == '') {
      this.controller.showToast('All fields are required to display chart', 'bottom');
      return false;
    }

    this.fundTypeSelector(this.fundType);
    let loader = this.controller.showLoader("Loading...");
    loader.present();

    let body = {
      RSAFund: this.rsaFund,
      RetireeFund: this.retireeFund,
      GuinessGratuityFund: this.guinessGratuityFund,
      duration: this.duration
    };
    var funcName = '/Chart';

    try {
      let serverResponse = await this.serverService.processData(body, funcName);
      console.log(serverResponse);

      if (serverResponse.StatusCode == "00") {
        if (serverResponse.Data != null) {
          let chartData = {
            label: serverResponse.Data.Datelabels,
            value: serverResponse.Data.values,
            chartType: this.chartType
          };
          this.controller.showModal(ChartModalPage, chartData);
        }
        else {
          this.controller.showToast('Not enough data to plot chart', 'bottom');
        }
      }
      else if (serverResponse.StatusCode == "FF"){
        this.controller.showToast(serverResponse.Message, 'bottom');
      }
    }
    catch(err) {
      console.log(err);
      this.controller.showToast('Connection error. Try again', 'bottom');
    }
    loader.dismiss();
  }

  fundTypeSelector(selectedFund) {
    if (selectedFund == "rsa") {
      this.rsaFund = true;
      this.retireeFund = false;
      this.guinessGratuityFund = false;
    }
    else if (selectedFund == "retiree") {
      this.rsaFund = false;
      this.retireeFund = true;
      this.guinessGratuityFund = false;
    }
    else {
      this.rsaFund = false;
      this.retireeFund = false;
      this.guinessGratuityFund = true;
    }
  }

  leavePage() {
    this.navCtrl.pop();
  }

}
