import { Component, ViewChild } from '@angular/core';
import { List, NavController, Platform, App } from 'ionic-angular';
import { ServerServiceProvider } from '../../providers/server-service/server-service';
import { ControllerService } from '../../providers/controller-service';
import { NativeService } from '../../providers/native-service';
import { BranchMapPage } from '../branch-map/branch-map';

@Component({
  selector: 'page-branches',
  templateUrl: 'branches.html',
})
export class BranchesPage {
  @ViewChild(List) list: List;
  branchData: any = [];

  constructor(
    private serverService: ServerServiceProvider,
    private controller: ControllerService,
    private native: NativeService,
    private navCtrl: NavController,
    private platform: Platform,
    private app: App
  ) {}

  ionViewDidLoad() {
    console.log('ionViewDidLoad BranchesPage');
    this.getBranchList();
  }

  ionViewDidEnter(){
    this.platform.registerBackButtonAction(() => {
      this.leavePage();
    });
  }

  showSlide() {
    this.controller.showToast('Slide left', 'middle');
  }

  async getBranchList() {
    this.list.closeSlidingItems();
    let loader = this.controller.showLoader("Loading...");
    loader.present();

    try {
      let serverResponse = await this.serverService.processData('', '/ListOfBranchDetails/');
      console.log(serverResponse);

      if (serverResponse.StatusCode == "00") {
        this.branchData = serverResponse.Data;
      } 
      else if (serverResponse.StatusCode == "FF"){
        this.controller.showToast(serverResponse.Message, 'bottom');
      }
    }
    catch(err) {
      console.log(err);
      this.controller.showToast("Unable to get branches. Try again", "bottom");
    }
    loader.dismiss();
  }

  async callPhoneNumber(phoneNo) {
    this.list.closeSlidingItems();
    try {
      let response = await this.native.callPhoneNumber(phoneNo);

      if (response == "Failed") {
        this.controller.showToast("Unable to call number. Try again", "bottom");
      }
    }
    catch(err) {
      this.controller.showToast("Unable to call number. Try again", "bottom");
    }
  }

  async sendSMS(phoneNo) {
    this.list.closeSlidingItems();
    try {
      let response = await this.native.sendSMS(phoneNo);

      if (response == "Failed") {
        this.controller.showToast("Unable to send SMS. Try again", "bottom");
      }
    }
    catch(err) {
      this.controller.showToast("Unable to open SMS app. Try again", "bottom");
    }
  }

  async sendEmail(emailAddress) {
    this.list.closeSlidingItems();
    try {
      let response = await this.native.sendEmail(emailAddress);

      if (response == "Failed") {
        this.controller.showToast("Unable to send mail. Try again", "bottom");
      }
    }
    catch(err) {
      this.controller.showToast("Unable to send mail. Try again", "bottom");
    }
  }

  async getDirection(lat, long, branchLocation) {
    this.list.closeSlidingItems();
    console.log(lat + ", " + long);
    this.app.getRootNav().push(BranchMapPage, {
      lat: lat,
      long: long,
      branchName: branchLocation
    });
  }

  leavePage() {
    this.navCtrl.pop();
  }

  doRefresh(val) {
    this.branchData = [];
    setTimeout(() => {
      val.complete();
    }, 1000);
    this.getBranchList();
  }

}
