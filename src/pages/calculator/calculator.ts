import { Component } from '@angular/core';
import { NavController, Platform } from 'ionic-angular';
import { AlertController, AlertOptions } from 'ionic-angular';
import { CalculatorModalPage } from '../calculator-modal/calculator-modal';
import { ControllerService } from '../../providers/controller-service';

@Component({
  selector: 'page-calculator',
  templateUrl: 'calculator.html',
})
export class CalculatorPage {
  alertOptions: AlertOptions;
  disclaimerMessage: string = "The Calculator on this mobile application is indicating and strictly for information purposes. Leadway Pensure PFA's obligations with respect to its services on this mobile application are governed solely by the agreements under which they are provided and nothing hereof should be construed to alter such service agreements.";

  constructor(
    private navCtrl: NavController,
    private alertCtrl: AlertController,
    private controller: ControllerService,
    public platform: Platform
  ) {}

  ionViewDidLoad() {
    console.log('ionViewDidLoad CalculatorPage');
    this.showDisclaimer();
  }

  ionViewDidEnter(){
    this.platform.registerBackButtonAction(() => {
      this.leavePage();
    });
  }

  showDisclaimer() {
    this.alertOptions = {
      title: 'DISCLAIMER',
      message: this.disclaimerMessage,
      enableBackdropDismiss: false,
      buttons: [
        {
          text: 'Disagree',
          handler: () => {
            this.navCtrl.pop();
          }
        },
        {
          text: 'Agree',
          role: 'cancel'
        }
      ]
    };
    let alert = this.alertCtrl.create(this.alertOptions);
    alert.present();
  }

  desiredPension() {
    let data = {
      pension: true,
      retire: false
    };
    this.controller.showModal(CalculatorModalPage, data);
  }

  retirementCollection() {
    let data = {
      pension: false,
      retire: true
    };
    this.controller.showModal(CalculatorModalPage, data);
  }

  leavePage() {
    this.navCtrl.pop();
  }

}
