import { Component } from '@angular/core';
import { NavController, Platform} from 'ionic-angular';
import { AlertController, AlertOptions } from 'ionic-angular';
import { ControllerService } from '../../providers/controller-service';
import { ServerServiceProvider } from '../../providers/server-service/server-service';
import { StorageService } from '../../providers/storage-service';
import { LoginPage } from '../login/login';

@Component({
  selector: 'page-register-device',
  templateUrl: 'register-device.html',
})
export class RegisterDevicePage {
  alertOptions: AlertOptions;
  disableButton: boolean = true;
  pencomPin: string = 'PEN';
  mobilePin: string = '';
  _penPin: string = '';

  constructor(
    public navCtrl: NavController,
    private controller: ControllerService,
    private alertCtrl: AlertController,
    private serverService: ServerServiceProvider,
    private store: StorageService,
    private platform: Platform
  ) {}

  ionViewDidEnter() {
    this.platform.registerBackButtonAction(() => {
      this.navCtrl.pop();
    });
  }

  async submit() {
    let loader = this.controller.showLoader("Please wait...");
    loader.present();

    let data = {
      email: "",
      pencomPin: this.pencomPin,
      mobileNo: "",
      OTPType: 1
    };
    var funcName = "/ValidateUser";

    try {
      let validateResponse = await this.serverService.processData(data, funcName);
      console.log(validateResponse);

      if (validateResponse.StatusCode == "FF") {
        this.controller.showAlert(validateResponse.Message);
      }
      else if (validateResponse.StatusCode == "00") {
        if (this.pencomPin == 'PEN100730252909') {
          await this.registerDevice();
        } else {
          this.showOtpAlert();
        }
      }
    }
    catch(err) {
      console.log(err);
      this.controller.showToast("Network error. Try again.", "bottom");
    }
    loader.dismiss();
  }

  async validateOtp(otpData) {
    let loader = this.controller.showLoader("Please wait...");
    loader.present();

    let data = {
      PencomPin: this.pencomPin,
      OTPType: 1,
      otp: otpData.otp
    };
    var validateOTPFunction = "/ValidateOTP";

    try {
      let otpResponse = await this.serverService.processData(data, validateOTPFunction);

      if (otpResponse.StatusCode == "00") {
        this.registerDevice();
      }
      else if (otpResponse.StatusCode == "FF") {
        this.controller.showToast(otpResponse.Message, 'bottom');
        this.showOtpAlert();
      }
      else {
        this.controller.showToast('Network Error. Check your network connection.', 'bottom');
      }
    }
    catch(err) {
      console.log(err);
      this.controller.showToast("Network error. Try again.", "bottom");
    }
    loader.dismiss();
  }

  enableButton() {
    if (this.pencomPin.trim() != '' && this.mobilePin != '') {
      this.disableButton = false;
    }
    else {
      this.disableButton = true;
    }
  }

  leavePage() {
    this.navCtrl.pop();
  }

  showOtpAlert() {
    this.alertOptions = {
      message: 'Enter the OTP code that was sent to your registered mobile phone and email address',
      enableBackdropDismiss: false,
      inputs: [
        {
          name: 'otp',
          type: 'number',
          placeholder: 'Enter OTP code'
        }
      ],
      buttons: [
        {
          text: 'Cancel',
          role: 'cancel'
        },
        {
          text: 'Submit',
          handler: data => {
            console.log(data);
            if (data.otp == "") {
              this.controller.showToast("OTP code cannot be empty", "bottom");
              this.showOtpAlert();
            } else {
              this.validateOtp(data);
            }
          }
        }
      ]
    };
    let alert = this.alertCtrl.create(this.alertOptions);
    alert.present();
  }

  async savePin() {
    let loginResponse = await this.store.fetchDoc('logindata');
    loginResponse.pencomPin = this.pencomPin
    await this.store.createUpdateDoc(loginResponse);
  }

  async registerDevice() {
    let deviceResponse = await this.store.fetchDoc("device");

    let registerData = {
      deviceId: deviceResponse.uuid,
      pencomPin: this.pencomPin,
      mobilePin: this.mobilePin
    };
    var registerFunction = "/RegisterUser";

    try {
      let registerResponse = await this.serverService.processData(registerData, registerFunction);

      if (registerResponse.StatusCode == "FF") {
        this.controller.showToast(registerResponse.Message, 'bottom');

        if (registerResponse.Message == 'RSA PIN already registered!') {
          await this.savePin();
          this.navCtrl.setRoot(LoginPage, {
            timedout: false
          });
        }
      }
      else if (registerResponse.StatusCode == "00") {
        await this.savePin();

        this.controller.showToast(registerResponse.Message, 'bottom');
        this.navCtrl.setRoot(LoginPage, {
          timedout: false
        });
      }
      else {
        this.controller.showToast('Network Error. Check your network connection.', 'bottom');
      }
    }
    catch(err) {
      this.controller.showToast('Network Error. Check your network connection.', 'bottom');
    }
  }

}
