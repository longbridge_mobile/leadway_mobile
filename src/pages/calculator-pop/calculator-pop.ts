import { Component } from '@angular/core';
import { ViewController, NavParams } from 'ionic-angular';

@Component({
  selector: 'page-calculator-pop',
  templateUrl: 'calculator-pop.html',
})
export class CalculatorPopPage {
  label1: string = "";
  label2: string = "";
  value1: string = "";
  value2: string = "";

  constructor(public viewCtrl: ViewController, public navParams: NavParams) {}

  ionViewDidLoad() {
    console.log('ionViewDidLoad CalculatorPopPage');
    this.label1 = this.navParams.get("labelName1");
    this.label2 = this.navParams.get("labelName2");
    this.value1 = this.navParams.get("value1");
    this.value2 = this.navParams.get("value2");
  }

  close() {
    this.viewCtrl.dismiss();
  }

}
