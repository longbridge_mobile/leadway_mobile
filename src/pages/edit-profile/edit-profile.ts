import { Component } from '@angular/core';
import { NavController, Platform } from 'ionic-angular';
import { StorageService } from '../../providers/storage-service';
import { ServerServiceProvider } from '../../providers/server-service/server-service';
import { AppDataServiceProvider } from '../../providers/app-data-service/app-data-service';
import { ControllerService } from '../../providers/controller-service';

@Component({
  selector: 'page-edit-profile',
  templateUrl: 'edit-profile.html',
})
export class EditProfilePage {
  email: string = '';
  mobileNo: string = '';
  title: string = '';
  gender: string = '';
  lga: string = '';
  stateOforigin: string = '';
  lgaData: any = [];
  
  constructor(
    public navCtrl: NavController,
    private store: StorageService,
    private serverService: ServerServiceProvider,
    private controller: ControllerService,
    private platform: Platform,
    private appService: AppDataServiceProvider
  ) {}

  ionViewDidLoad() {
    console.log('ionViewDidLoad EditProfilePage');
    this.fetchExistingProfile();
  }

  ionViewDidEnter(){
    this.platform.registerBackButtonAction(() => {
      this.leavePage();
    });
  }

  async fetchExistingProfile() {
    let userResponse = await this.store.fetchDoc('user');

    if (userResponse != "Failed") {
      this.email = userResponse.email;
      this.mobileNo = userResponse.mobileNo;
      this.title = userResponse.title;
      this.gender = userResponse.gender;
      this.lga = userResponse.lga;
      this.stateOforigin = userResponse.stateOfOrigin;
      this.lgaData = this.appService.getLGA().filter((item) => {
        return item.value === this.stateOforigin.toUpperCase();
      });
      console.log(this.lgaData);
    }
    else {
      this.fetchServerProfile();
    }
  }

  async fetchServerProfile() {
    let loader = this.controller.showLoader('Getting details...');
    loader.present();

    let loginResponse = await this.store.fetchDoc('logindata');
    let body = {
      sessionID: loginResponse.sessionId
    };
    var funcName = '/FullClientDetails';

    try {
      let serverResponse = await this.serverService.processData(body, funcName);
      console.log(serverResponse);

      if (serverResponse.StatusCode == "00") {
        this.mobileNo = serverResponse.Data.mobileno;
        this.email = serverResponse.Data.emailaddres;
        this.title = serverResponse.Data.title;
        this.gender = serverResponse.Data.sex;
        this.lga = serverResponse.Data.lgastateoforigin;
        this.stateOforigin = serverResponse.Data.stateoforigjn;
        this.lgaData = this.appService.getLGA().filter((item) => {
          return item.value === this.stateOforigin.toUpperCase();
        });
        console.log(this.lgaData);
      }
      else if (serverResponse.StatusCode == "FF") {
        this.controller.showToast(serverResponse.Message, 'bottom');
      }
      else {
        this.controller.showToast('Network error. Check your network connection.', 'bottom');
      }
    }
    catch(err) {
      console.log(err);
      this.controller.showToast('Unable to fetch existing details', 'bottom');
    }
    loader.dismiss();
  }

  async updateProfile() {
    let loader = this.controller.showLoader('Submitting update...');
    loader.present();

    let loginResponse = await this.store.fetchDoc('logindata');
    let deviceResponse = await this.store.fetchDoc('device');

    let body = {
      Email: this.email,
      Mobile: this.mobileNo,
      PencomPin: loginResponse.pencomPin,
      mobileId: deviceResponse.uuid,
      Title: this.title,
      Gender: this.gender,
      LGA: this.lga
    };

    var funcName = '/ChangePersonalDetails';

    try {
      let serverResponse = await this.serverService.processData(body, funcName);
      console.log(serverResponse);

      if (serverResponse.StatusCode == '00') {
        this.controller.showToast(serverResponse.Message, 'bottom');
      } 
      else if (serverResponse.StatusCode == 'FF') {
        this.controller.showToast(serverResponse.Message, 'bottom');
      }
      else {
        this.controller.showToast('Unable to update details. Try again later.', 'bottom');
      }
    }
    catch(err) {
      console.log(err);
      this.controller.showToast('Unable to update details. Try again later.', 'bottom');
    }
    loader.dismiss();
  }

  doRefresh(val) {
    setTimeout(() => {
      val.complete();
    }, 1000);
    this.fetchServerProfile();
  }

  leavePage() {
    this.navCtrl.pop();
  }

}
