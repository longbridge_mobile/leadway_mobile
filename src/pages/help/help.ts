import { Component } from '@angular/core';
import { NavController, Platform } from 'ionic-angular';
import { FaqPage } from '../faq/faq';
import { BranchesPage } from '../branches/branches';
import { ResetPinPage } from '../reset-pin/reset-pin';
import { AboutPage } from '../about/about';

@Component({
  selector: 'page-help',
  templateUrl: 'help.html',
})
export class HelpPage {

  constructor(public navCtrl: NavController, public platform: Platform) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad HelpPage');
  }

  ionViewDidEnter(){
   this.platform.registerBackButtonAction(() => {
     this.leavePage();
   });
  }

  leavePage() {
    this.navCtrl.pop();
  }

  resetPin() {
    this.navCtrl.push(ResetPinPage);
  }

  faq() {
    this.navCtrl.push(FaqPage);
  }

  branches() {
    this.navCtrl.push(BranchesPage);
  }

  about() {
    this.navCtrl.push(AboutPage);
  }

}
