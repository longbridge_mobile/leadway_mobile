import { Component } from '@angular/core';
import { NavController, Platform } from 'ionic-angular';
import { ControllerService } from '../../providers/controller-service';
import { StorageService } from '../../providers/storage-service';
import { NativeService } from '../../providers/native-service';
import { ServerServiceProvider } from '../../providers/server-service/server-service';
import { TransactionPage } from '../transaction/transaction';
import { DatePicker, DatePickerOptions } from '@ionic-native/date-picker';

@Component({
  selector: 'page-search-transaction',
  templateUrl: 'search-transaction.html',
})
export class SearchTransactionPage {
  startDate: string = "";
  _startDate: string = "01-JAN-1970";
  endDate: string = "";
  _endDate: string = "31-DEC-2100";
  accountType: string = "RSA";
  transactionData: any = [];
  
  monthList = ['JAN', 'FEB', 'MAR', 'APR', 'MAY', 'JUN', 'JUL', 'AUG', 'SEP', 'OCT', 'NOV', 'DEC'];

  constructor(
    public navCtrl: NavController,
    private controller: ControllerService,
    private store: StorageService,
    private serverService: ServerServiceProvider,
    private native: NativeService,
    private platform: Platform,
    private datePicker: DatePicker
  ) {}

  ionViewDidLoad() {
    console.log('ionViewDidLoad SearchTransactionPage');
  }

  ionViewDidEnter(){
   this.platform.registerBackButtonAction(() => {
     this.leavePage();
   });
  }

  async showTransactions() {

    if (new Date(this._endDate) < new Date(this._startDate)) {
      this.controller.showToast('Start date cannot be greater than end date', 'bottom');
      return false;
    }

    var RSA = true;
    var retiree = true;
    var gratuity = true;

    let loader = this.controller.showLoader("Fetching transactions...");
    loader.present();

    let loginResponse = await this.store.fetchDoc("logindata");
    console.log(loginResponse);

    let data = {
      GuinessGratuityFund: RSA,
      RetireeFund: retiree,
      RSAFund: gratuity,
      startDate: this.startDate,
      endDate: this.endDate,
      sessionID: loginResponse.sessionId
    };
    var funcName = "/DetailedAccountStatement";
    console.log(data);

    try {
      let serverResponse = await this.serverService.processData(data, funcName);
      console.log(serverResponse);

      if (serverResponse.StatusCode == "FF") {
        this.controller.showToast(serverResponse.Message, 'bottom');
      }
      else if (serverResponse.StatusCode == "00") {
        this.transactionData = serverResponse.Data.funds_account_details.transactionsField.reverse();
        // for (let val of serverResponse.Data.funds_account_details.transactionsField.reverse()) {
        //   if ((new Date(val.dateField) >= new Date(this._startDate)) && (new Date(val.dateField) <= new Date(this._endDate))) {
        //     this.transactionData.push(val);
        //   }
        // }

        this.navCtrl.push(TransactionPage, {
          transactionData: this.transactionData
        });
      }
    }
    catch(err) {
      console.log(err);
      this.controller.showToast("Network error. Try again.", "bottom");
    }
    loader.dismiss();
  }

  pickDate(dateType, selectedDate) {
    var pickedDate;
    if (selectedDate == '') {
      pickedDate = new Date();
    }
    else {
      pickedDate = (dateType == 'startdate') ? new Date(this._startDate) : new Date(this._endDate);
    }

    let dateOptions: DatePickerOptions = {
      mode: "date",
      date: pickedDate,
      androidTheme: this.datePicker.ANDROID_THEMES.THEME_HOLO_LIGHT
    };

    this.datePicker.show(dateOptions).then((val) => {
      var dayDate = (val.getDate() < 10) ? '0'+val.getDate() : val.getDate().toString();
      // var monthDate = (val.getMonth() < 9) ? '0'+(val.getMonth() + 1) : (val.getMonth() + 1).toString();
      var monthDate = this.monthList[val.getMonth()];
      var fullYear = val.getFullYear().toString();

      if (dateType == "startdate") {
        this.startDate = dayDate + '-' + monthDate + '-' + fullYear;
        this._startDate = val.toDateString();
      }
      else {
        this.endDate = dayDate + '-' + monthDate + '-' + fullYear;
        this._endDate = val.toDateString();
      }
    })
    .catch((err) => {
      return '';
    });
  }

  leavePage() {
    this.navCtrl.pop();
  }

}
