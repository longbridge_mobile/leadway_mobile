import { Component, ViewChild } from '@angular/core';
import { ViewController } from 'ionic-angular';
import { SignaturePad } from 'angular2-signaturepad/signature-pad';

@Component({
  selector: 'page-signature-pad',
  templateUrl: 'signature-pad.html',
})
export class SignaturePadPage {
  signature: string = '';
  isDrawing = false;
  @ViewChild(SignaturePad) signaturePad: SignaturePad;

  private signaturePadOptions: Object = {
    'minWidth': 2,
    'penColor': '#000000',
    'canvasWidth': 300,
    'canvasHeight': 400
  };

  constructor(public viewCtrl: ViewController) {}

  ionViewDidLoad() {
    console.log('ionViewDidLoad SignaturePadPage');
    this.clear();
  }

  drawStart() {
    this.isDrawing = true;
  }

  drawComplete() {
    this.isDrawing = false;
  }

  save() {
    this.signature = this.signaturePad.toDataURL();
    this.leavePage(this.signature);
    this.clear();
  }

  clear() {
    this.signaturePad.clear();
  }

  leavePage(val) {
    this.clear();
    this.viewCtrl.dismiss(val)
  }

}
