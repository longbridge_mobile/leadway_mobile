import { Component } from '@angular/core';
import { NavParams, ViewController, Platform } from 'ionic-angular';
import { StorageService } from '../../providers/storage-service';
import { ServerServiceProvider } from '../../providers/server-service/server-service';
import { ControllerService } from '../../providers/controller-service';

@Component({
  selector: 'page-faq-modal',
  templateUrl: 'faq-modal.html',
})
export class FaqModalPage {
  name: any;
  details: any;
  title: any;
  sadColor: string = 'tertiary';
  happyColor: string = 'tertiary';
  feedbackMessage: string = "Was this helpful?";
  response: boolean = false;
  showSad: boolean = true;
  showHappy: boolean = true;

  constructor(public viewCtrl: ViewController, 
    public navParams: NavParams,
    private store: StorageService,
    private platform: Platform,
    private controller: ControllerService,
    private serverService: ServerServiceProvider
  ) {}

  ionViewDidLoad() {
    console.log('ionViewDidLoad FaqModalPage');
    this.name = this.navParams.get('name');
    this.details = this.navParams.get('details');
    this.title = this.navParams.get('title');
    
    this.getFeedback();
  }

  ionViewDidEnter(){
    this.platform.registerBackButtonAction(() => {
      this.close();
    });
  }

  async getFeedback() {
    let prodResponse = await this.store.fetchDoc('feedback');

    for (let val of prodResponse.feedback) {
      if (val.content == this.details) {
        this.response = val.response;
        break;
      }
    }
  }

  async feedbackEvent(val) {
    if (this.sadColor == this.happyColor) {
      this.feedbackMessage = "Thanks for your feedback";
      if (val == "sad") {
        this.showSad = true;
        this.showHappy = false;
        this.submitFeedback(0);
      }
      else {
        this.showSad = false;
        this.showHappy = true;
        this.submitFeedback(1);
      }

      let response = await this.store.fetchDoc('feedback');

      if (response != 'Failed') {
        response.feedback.push({
          content: this.details,
          response: true
        });

        await this.store.createUpdateDoc(response);
      }
      else {
        let doc = {
          _id: 'feedback',
          feedback: [{
            content: this.details,
            response: true
          }]
        };
        await this.store.createUpdateDoc(doc);
      }
    }
  }

  async submitFeedback(status) {
    let data = {
      FeedbackType: this.title,
      Title: this.name,
      FeedbackStatus: status
    };
    var funcName = '/Feedback';

    try {
      await this.serverService.processData(data, funcName);
    }
    catch(err) {
      console.log(err);
    }
  }

  close() {
    if (FaqModalPage){
      this.viewCtrl.dismiss();
    }
    else {
      this.controller.closeAppAlert();
    }
  }

}
