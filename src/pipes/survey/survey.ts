import { Pipe, PipeTransform } from '@angular/core';

/**
 * Generated class for the SurveyPipe pipe.
 *
 * See https://angular.io/docs/ts/latest/guide/pipes.html for more info on
 * Angular Pipes.
 */
@Pipe({
  name: 'survey',
})
export class SurveyPipe implements PipeTransform {
  transform(surveyData: any[], args: string): any {
    return surveyData.filter(res => res.Question === args);
  }
}
