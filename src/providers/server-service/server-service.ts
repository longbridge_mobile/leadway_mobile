import { Injectable } from '@angular/core';
import { Http, Headers } from '@angular/http';
import 'rxjs/add/operator/toPromise';
import { NativeService } from '../native-service';

@Injectable()
export class ServerServiceProvider {
  // url: string = "https://mapps.leadway-pensure.com/LeadwayMobileApplicationNewUAT/PensureMobile";
  url: string = "https://mapps.leadway-pensure.com/LeadwayMobileApplicationWebapi/PensureMobile";

  constructor(private http: Http, private native: NativeService) {
    console.log('Hello ServerServiceProvider Provider');
  }

  async processData(body, funcName): Promise<any> {
    let appKeyString = this.appKeyHeader();
    let headers = new Headers();
    headers.append('Content-Type', 'application/json');
    headers.append('AppKey', appKeyString);
    headers.append('AppVersion', '2.0.0');

    try {
      let response = await this.http.post(this.url + funcName, JSON.stringify(body), {headers: headers}).toPromise();
      return response.json();
    }
    catch(err) {
      console.log(err.json());
      return {StatusCode: "FF", Message: err.json().Message};
    }
  }

  appKeyHeader(): string {
    let date = new Date();
    var month = (date.getMonth() < 9) ? '0' + (date.getMonth() + 1).toString() : (date.getMonth() + 1).toString();
    var day = (date.getDate() < 10) ? '0' + date.getDate().toString() : date.getDate().toString();
    var minutes = (date.getMinutes() < 10) ? '0' + date.getMinutes().toString() : date.getMinutes().toString();
    var seconds = (date.getSeconds() < 10) ? '0' + date.getSeconds().toString() : date.getSeconds().toString();

    let currentDate = day + '-' + month + '-' + date.getFullYear().toString();
    let currentTime = date.getHours().toString() + ':' + minutes + ':' + seconds;
    console.log(`c457864y95tcy458tu98tcrtcer9mth9|${currentDate}${currentTime}`);
    // console.log(`12345|${currentDate}${currentTime}`);

    // return btoa(`12345|${currentDate}${currentTime}`);
    return btoa(`c457864y95tcy458tu98tcrtcer9mth9|${currentDate}${currentTime}`);
  }

}
