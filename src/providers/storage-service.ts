import { Injectable } from '@angular/core';
import PouchDB from 'pouchdb';

@Injectable()
export class StorageService {
  db: any;

  constructor() {
    console.log('Hello StorageService Provider');
    this.db = new PouchDB('leadway');
  }

  async createUpdateDoc(doc) {
    try {
      const response = await this.db.put(doc);
      return response;
    }
    catch(err) {
      console.log(err);
      return 'Failed';
    }
  }

  async fetchDoc(docId) {
    try {
      const response = await this.db.get(docId);
      return response;
    }
    catch(err) {
      console.log(err);
      return 'Failed';
    }
  }

  async deleteDoc(doc) {
    try {
      const response = await this.db.remove(doc);
      return response;
    }
    catch(err) {
      console.log(err);
      return 'Failed';
    }
  }

  async bulkCreateDoc(arrayDoc) {
    try {
      const response = await this.db.bulkDocs(arrayDoc);
      return response;
    }
    catch(err) {
      console.log(err);
      return 'Failed';
    }
  }

}
