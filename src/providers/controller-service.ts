import { Injectable } from '@angular/core';
import { ToastController, ToastOptions, Platform } from 'ionic-angular';
import { ModalController, ModalOptions } from 'ionic-angular';
import { LoadingController, LoadingOptions } from 'ionic-angular';
import { AlertController, AlertOptions } from 'ionic-angular';

@Injectable()
export class ControllerService {
  toastOptions: ToastOptions;
  modalOptions: ModalOptions;
  loadingOptions: LoadingOptions;

  constructor(
    private toastCtrl: ToastController,
    private modalCtrl: ModalController,
    private loadCtrl: LoadingController,
    public platform: Platform,
    private alertCtrl: AlertController
  ) {
    console.log('Hello ControllerService Provider');
  }

  showToast(toastMsg, position) {
    this.toastOptions = {
      message: toastMsg,
      duration: 3000,
      position: position
    };
    this.toastCtrl.create(this.toastOptions).present();
  }

  showModal(pageName, modalData) {
    this.modalOptions = {
      enableBackdropDismiss: false
    };
    this.modalCtrl.create(pageName, modalData, this.modalOptions).present();
  }

  showBankModal(pageName, modalData) {
    this.modalOptions = {
      enableBackdropDismiss: false
    };
    return this.modalCtrl.create(pageName, modalData, this.modalOptions);
  }

  showLoader(loadingMsg: string): any {
    this.loadingOptions = {
      content: loadingMsg
    };
    return this.loadCtrl.create(this.loadingOptions);
  }

  showAlert(message) {
    let alertOptions: AlertOptions = {
      enableBackdropDismiss: false,
      subTitle: message,
      buttons: [
        {
          text: 'OK',
          role: 'cancel'
        }
      ]
    };
    this.alertCtrl.create(alertOptions).present();
  } 

  closeAppAlert() {
    let alertOptions: AlertOptions = {
      enableBackdropDismiss: false,
      subTitle: 'Are you sure you want to exit the app?',
      buttons: [
        {
          text: 'No',
          role: 'cancel'
        },
        {
          text: 'Yes',
          handler: () => {
            this.platform.exitApp();
          }
        }
      ]
    };
    this.alertCtrl.create(alertOptions).present();
  }

}
