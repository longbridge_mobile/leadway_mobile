import { Injectable } from '@angular/core';
import { BackgroundMode } from '@ionic-native/background-mode';
import { CallNumber } from '@ionic-native/call-number';
import { Camera, CameraOptions } from '@ionic-native/camera';
import { FingerprintAIO, FingerprintOptions } from '@ionic-native/fingerprint-aio';
import { SocialSharing } from '@ionic-native/social-sharing';
import { Device } from '@ionic-native/device';
import { HeaderColor } from '@ionic-native/header-color';
import { Keyboard } from '@ionic-native/keyboard';
import { AppVersion } from '@ionic-native/app-version';
import { StorageService } from './storage-service';
import { AppDataServiceProvider } from './app-data-service/app-data-service';

@Injectable()
export class NativeService {
  private printOptions: FingerprintOptions;

  constructor(
    private backMode: BackgroundMode,
    private callNumber: CallNumber,
    private camera: Camera,
    private fingerprint: FingerprintAIO,
    private socialSharing: SocialSharing,
    private device: Device,
    private headerColor: HeaderColor,
    private keyboard: Keyboard,
    private appVersion: AppVersion,
    private store: StorageService,
    private appServer: AppDataServiceProvider
  ) {
    console.log('Hello NativeService Provider');
  }

  async runAppInBackground() {
    try {
      const response = await this.backMode.enable();
      return response;
    }
    catch(err) {
      console.log(err);
    }
  }

  async callPhoneNumber(phoneNo: string) {
    try {
      await this.callNumber.callNumber(phoneNo, true);
    }
    catch(err) {
      console.log(err);
      return 'Failed';
    }
  }

  async sendEmail(email: string) {
    try {
      const response = await this.socialSharing.shareViaEmail('','',[email]);
      return response;
    }
    catch(err) {
      console.log(err);
      return 'Failed';
    }
  }

  async sendSMS(phoneNo: string) {
    try {
      const response = await this.socialSharing.shareViaSMS('',phoneNo);
      return response;
    }
    catch(err) {
      console.log(err);
      return 'Failed';
    }
  }

  async uploadFromLibrary() {
    console.log('From Photo Library');
    try {
      let cameraOptions: CameraOptions = {
        destinationType: this.camera.DestinationType.DATA_URL,
        sourceType: this.camera.PictureSourceType.PHOTOLIBRARY,
        targetHeight: 800,
        targetWidth: 800,
        quality: 100
      };
      const response = await this.camera.getPicture(cameraOptions);
      return 'data:image/jpeg;base64,' + response;
    }
    catch(err) {
      console.log(err);
       return this.appServer.leadwayImage();
    }
  }

  async uploadFromCamera() {
    console.log('From Camera');
    try {
      let cameraOptions: CameraOptions = {
        destinationType: this.camera.DestinationType.DATA_URL,
        sourceType: this.camera.PictureSourceType.CAMERA,
        targetHeight: 800,
        targetWidth: 800,
        quality: 100
      };
      const response = await this.camera.getPicture(cameraOptions);
      return 'data:image/jpeg;base64,' + response;
    }
    catch(err) {
      console.log(err);
      return this.appServer.leadwayImage();
    }
  }

  async setHeaderColor() {
    try {
      await this.headerColor.tint('#f8822c');
    }
    catch(err) {
      console.log(err);
    }
  }

  async getDeviceInfo() {
    let uuid: string = '';
    let model: string = '';
    let platform: string = '';

    try {
      uuid = this.device.uuid;
      platform = this.device.platform;
      model = this.device.model;
    } catch (error) {
      console.log(error);
      uuid = null;
    }

    if (uuid == null) {
      uuid = 'ID' + (Math.floor(Math.random() * (1000000 - 100000)) + 100000);
    }

    let data = {
      "_id": "device",
      "uuid": uuid,
      "model": model,
      "os": platform
    };
    await this.store.createUpdateDoc(data);
    return uuid;
  }

  async scanFingerprint() {
    try {
      this.printOptions = {
        clientId: 'Leadway',
        clientSecret: 'password',
        disableBackup: true
      };
      let available = await this.fingerprint.isAvailable();

      if (available === "OK") {
        let response = await this.fingerprint.show(this.printOptions);
        return response;
      } else {
        return "Cancel";
      }
    }
    catch(err) {
      console.log(err);
      return 'Failed';
    }
  }

  async getAppVersion() {
    try {
      let version = await this.appVersion.getVersionNumber();
      return version;
    } catch (error) {
      return '12345';
    }
  }

}
